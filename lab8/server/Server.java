package lab8.server;

import lab8.server.model.Game;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    private static final int PORT = 8100;
    private List<Game> gameList = new ArrayList<>();

    private Server() throws IOException {
        try (ServerSocket serverSocket = new ServerSocket(PORT)) {
            while (true) {
                System.out.println("Waiting for client...");
                Socket socket = serverSocket.accept();
                new ClientThread(this, socket).start();
            }
        } catch (IOException e) {
            System.err.println("Ooops... " + e);
        }
    }

    public static void main(String[] args) throws IOException {
        new Server();
    }

    public List<Game> getGameList() {
        return gameList;
    }

    public void setGameList(List<Game> gameList) {
        this.gameList = gameList;
    }
}
