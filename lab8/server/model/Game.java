package lab8.server.model;

import lab7.model.player.Player;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Game implements Serializable{

    private Player admin;
    private String gameName;
    private List<Player> playerList = new ArrayList<>();

    public Game(Player admin, String gameName) {
        this.setAdmin(admin);
        this.setGameName(gameName);
        getPlayerList().add(admin);
    }



    @Override
    public String toString() {
        return "Game{" +
                "admin=" + getAdmin() +
                ", gameName='" + getGameName() + '\'' +
                ", playerList=" + getPlayerList() +
                '}';
    }

    public Player getAdmin() {
        return admin;
    }

    public void setAdmin(Player admin) {
        this.admin = admin;
    }

    public String getGameName() {
        return gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public List<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(List<Player> playerList) {
        this.playerList = playerList;
    }
}
