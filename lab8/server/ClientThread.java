package lab8.server;

import lab8.client.model.Message;
import lab8.server.model.Game;

import java.io.*;
import java.net.Socket;

public class ClientThread extends Thread {

    private Server server;
    private Socket socket = null;

    public ClientThread(Server server, Socket socket) {
        this.server = server;
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            while (true) {
                ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());

                Message message = (Message) in.readObject();
                switch (message.getActionName()) {
                    case "createGame": {
                        Game game = new Game(message.getPlayer(), message.getValue());
                        server.getGameList().add(game);
                    }
                    case "joinGame": {
                        String gameName = message.getValue();
                        Game game = findGame(gameName);
                        out.writeObject(game);

                    }
                }
            }
        } catch (IOException e) {
            System.err.println("Communication error... " + e);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                System.err.println(e);
            }
        }
    }

    private Game findGame(String gameName) {
        for (Game game : server.getGameList()) {
            if(game.getGameName().equals(gameName)) return game;
        }
        return null;
    }
}
