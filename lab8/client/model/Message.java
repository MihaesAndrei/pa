package lab8.client.model;

import lab7.model.player.Player;

import java.io.Serializable;

public class Message implements Serializable{

    private Player player;
    private String actionName;
    private String value;

    public Message(Player player, String actionName, String value) {
        this.player = player;
        this.actionName = actionName;
        this.value = value;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Message{" +
                "player=" + player +
                ", actionName='" + actionName + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
