package lab8.client;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lab7.model.player.Player;
import lab8.client.views.layouts.ConnectLayout;

import java.net.Socket;

public class Client extends Application {

    private Stage primaryStage = null;
    private Socket socket = null;
    private Player player = new Player("");

    public static void main(String[] args) { Application.launch(args); }

    @Override
    public void start(Stage primaryStage) throws Exception {

        this.primaryStage = primaryStage;
        primaryStage.setTitle("Word Game Online");

        ConnectLayout connectLayout = new ConnectLayout(this);
        primaryStage.setScene(new Scene(connectLayout, 700, 500));
        primaryStage.show();
    }
    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}