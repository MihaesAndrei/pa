package lab8.client.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import lab8.client.Client;
import lab8.client.views.alerts.ServerDownAlert;
import lab8.client.views.layouts.MenuLayout;

import java.io.*;
import java.net.Socket;
import java.util.Properties;

public class ConnectController implements EventHandler<ActionEvent> {

    private Client client;
    private TextField name;

    public ConnectController(Client client, TextField name) {
        this.client = client;
        this.name = name;
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            InputStream inputStream = new FileInputStream("src/lab8/client/resources/server.properties");
            Properties properties = new Properties();
            properties.load(inputStream);

            client.setSocket(new Socket(properties.getProperty("ipAddress"), Integer.parseInt(properties.getProperty("port"))));

//            set player name
            client.getPlayer().setName(name.getText());

            client.getPrimaryStage().setScene(new Scene(new MenuLayout(client), 700, 500));

        } catch (IOException e) {
            ServerDownAlert serverDownAlert = new ServerDownAlert(Alert.AlertType.WARNING);
            serverDownAlert.showAndWait();
        }
    }
}
