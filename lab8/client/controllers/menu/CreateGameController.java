package lab8.client.controllers.menu;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import lab8.client.Client;
import lab8.client.model.Message;
import lab8.client.views.alerts.GameCreatedAlert;

import java.io.ObjectOutputStream;

public class CreateGameController implements EventHandler<ActionEvent> {

    private Client client;
    private TextField gameNameField;

    public CreateGameController(Client client, TextField gameNameField) {
        this.client = client;
        this.gameNameField = gameNameField;
    }


    @Override
    public void handle(ActionEvent event) {
        try {
            ObjectOutputStream writer = new ObjectOutputStream(client.getSocket().getOutputStream());
            writer.writeObject(new Message(client.getPlayer(), "createGame", gameNameField.getText()));
            GameCreatedAlert gameCreatedAlert = new GameCreatedAlert(Alert.AlertType.INFORMATION);
            gameCreatedAlert.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
