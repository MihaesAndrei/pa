package lab8.client.controllers.menu;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import lab8.client.Client;
import lab8.client.model.Message;
import lab8.client.views.alerts.GameCreatedAlert;
import lab8.client.views.alerts.InvalidGameAlert;
import lab8.client.views.layouts.GameLayout;
import lab8.server.model.Game;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class JoinGameController implements EventHandler<ActionEvent> {


    private Client client;
    private TextField gameNameField;

    public JoinGameController(Client client, TextField gameNameField) {
        this.client = client;
        this.gameNameField = gameNameField;
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            ObjectOutputStream writer = new ObjectOutputStream(client.getSocket().getOutputStream());
            writer.writeObject(new Message(client.getPlayer(), "joinGame", gameNameField.getText()));

            ObjectInputStream reader = new ObjectInputStream(client.getSocket().getInputStream());
            Game game = (Game) reader.readObject();
            if (game == null) {
                InvalidGameAlert invalidGameAlert = new InvalidGameAlert(Alert.AlertType.WARNING);
                invalidGameAlert.showAndWait();
            }
            else {
                client.getPrimaryStage().setScene(new Scene(new GameLayout(client), 700, 500));
            }


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
