package lab8.client.views.buttons;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import lab8.client.Client;
import lab8.client.controllers.ConnectController;


public class ConnectButton extends Button {

    public ConnectButton(Client client, TextField name) {
        super();
        this.setText("Connect to server");
        this.setOnAction(new ConnectController(client, name));
    }
}
