package lab8.client.views.buttons.menu;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import lab8.client.Client;
import lab8.client.controllers.menu.CreateGameController;


public class CreateGameButton extends Button {

    public CreateGameButton(Client client, TextField gameNameField) {
        super();
        this.setText("Create a new game");
        this.setOnAction(new CreateGameController(client, gameNameField));
    }
}