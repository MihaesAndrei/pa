package lab8.client.views.buttons.menu;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import lab8.client.Client;
import lab8.client.controllers.menu.JoinGameController;

public class JoinGameButton extends Button {

    public JoinGameButton(Client client, TextField gameNameField) {
        super();
        this.setText("Join game");
        this.setOnAction(new JoinGameController(client, gameNameField));
    }

}
