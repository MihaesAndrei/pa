package lab8.client.views.alerts;

import javafx.beans.NamedArg;
import javafx.scene.control.Alert;

public class ServerDownAlert extends Alert {

    public ServerDownAlert(@NamedArg("alertType") AlertType alertType) {
        super(alertType);
        this.setTitle("Server Information");
        this.setHeaderText(null);
        this.setContentText("Server is down...");
    }
}
