package lab8.client.views.alerts;

import javafx.beans.NamedArg;
import javafx.scene.control.Alert;

public class InvalidGameAlert extends Alert {

    public InvalidGameAlert(@NamedArg("alertType") AlertType alertType) {
        super(alertType);
        this.setTitle("Game Information");
        this.setHeaderText(null);
        this.setContentText("Invalid Game Name!");
    }
}
