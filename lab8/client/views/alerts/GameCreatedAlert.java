package lab8.client.views.alerts;


import javafx.beans.NamedArg;
import javafx.scene.control.Alert;

public class GameCreatedAlert extends Alert {

    public GameCreatedAlert(@NamedArg("alertType") AlertType alertType) {
        super(alertType);
        this.setTitle("Game Information");
        this.setHeaderText(null);
        this.setContentText("Game was succesfully created!");
    }
}
