package lab8.client.views.layouts;

import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import lab8.client.Client;
import lab8.client.views.buttons.menu.CreateGameButton;
import lab8.client.views.buttons.menu.JoinGameButton;


public class MenuLayout extends GridPane {

    public MenuLayout(Client client) {
        TextField gameNameField = new TextField();
        this.add(new Label("Game name:  "), 0, 0);
        this.add(gameNameField, 1, 0);

        this.add(new Label(), 0, 1);

        this.add(new CreateGameButton(client, gameNameField), 0, 2);
        this.add(new JoinGameButton(client, gameNameField), 1, 2);

        this.setAlignment(Pos.CENTER);
    }

}
