package lab8.client.views.layouts;

import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import lab8.client.Client;

public class GameLayout extends GridPane {

    public GameLayout(Client client) {
        super();

        this.setAlignment(Pos.CENTER);
    }
}
