package lab8.client.views.layouts;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lab8.client.Client;
import lab8.client.views.buttons.ConnectButton;

import java.net.Socket;

public class ConnectLayout extends VBox {

    public ConnectLayout(Client client) {
        TextField name = new TextField();

        HBox hBox = new HBox();
        hBox.getChildren().add(new Label("Name: "));
        hBox.getChildren().add(name);
        hBox.setAlignment(Pos.CENTER);

        this.getChildren().add(new ConnectButton(client, name));
        this.getChildren().add(new Label());
        this.getChildren().add(hBox);

        this.setAlignment(Pos.CENTER);
    }
}
