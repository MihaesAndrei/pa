package lab6.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import lab6.view.FunctionGraph;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

/**
 * Created by Andrei on 4/12/2016.
 */
public class DrawController implements EventHandler<ActionEvent> {

    private TextField functionTextField;
    private ColorPicker colorPicker;
    private StackPane layout;
    private TextField strokeField;

    public DrawController(TextField functionTextField, ColorPicker colorPicker, StackPane layout, TextField strokeField) {
        this.functionTextField = functionTextField;
        this.colorPicker = colorPicker;
        this.layout = layout;
        this.strokeField = strokeField;
    }

    @Override
    public void handle(ActionEvent event) {
        if (functionTextField.getText() == null || functionTextField.getText().equals("")) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Warning");
            alert.setHeaderText("Function field is empty!");

            alert.showAndWait();
        } else {
            try {
                Expression e1 = new ExpressionBuilder(functionTextField.getText())
                        .variables("x")
                        .build();
                FunctionGraph f1 = new FunctionGraph(
                        e1,
                        colorPicker.getValue().deriveColor(0, 1, 1, 0.6),
                        Integer.parseInt(strokeField.getText()
                        ));
                layout.getChildren().add(f1);
            } catch (IllegalArgumentException exception) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning");
                alert.setHeaderText("Invalid function");

                alert.showAndWait();
            }

        }
    }
}
