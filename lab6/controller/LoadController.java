package lab6.controller;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Andrei on 4/12/2016.
 */
public class LoadController implements EventHandler<ActionEvent> {

    private Stage stage;
    private StackPane layout;

    public LoadController(Stage stage, StackPane layout) {
        this.stage = stage;
        this.layout = layout;
    }

    @Override
    public void handle(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png");
        fileChooser.getExtensionFilters().add(extFilter);

        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            openFile(file);
        }
    }

    private void openFile(File file) {
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            WritableImage image = SwingFXUtils.toFXImage(bufferedImage, null);
            ImageView imageView = new ImageView(image);
            imageView.setFitWidth((int)layout.getWidth()-1);
            imageView.setFitHeight((int)layout.getHeight()-1);
            layout.getChildren().add(imageView);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
