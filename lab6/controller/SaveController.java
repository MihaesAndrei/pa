package lab6.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Alert;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Andrei on 4/12/2016.
 */
public class SaveController implements EventHandler<ActionEvent> {

    private Stage stage;
    private StackPane layout;


    public SaveController(Stage stage, StackPane layout) {
        this.stage = stage;
        this.layout = layout;
    }

    @Override
    public void handle(ActionEvent event) {
        try {
            FileChooser fileChooser = new FileChooser();

            //Set extension filter
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image files (*.png)", "*.png");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File file = fileChooser.showSaveDialog(stage);
            if (file != null) {
                String extension = FilenameUtils.getExtension(file.toString());
                BufferedImage bufferedImage = new BufferedImage((int) layout.getWidth(), (int) layout.getHeight(), BufferedImage.TYPE_INT_ARGB);
                WritableImage snapshot = layout.snapshot(new SnapshotParameters(), null);
                BufferedImage image = javafx.embed.swing.SwingFXUtils.fromFXImage(snapshot, bufferedImage);
                ImageIO.write(image, extension, file);

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Info");
                alert.setHeaderText("Imaged saved successfully");

                alert.showAndWait();
            }

        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
