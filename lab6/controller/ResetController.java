package lab6.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.StackPane;
import lab6.view.FunctionGraph;

public class ResetController implements EventHandler<ActionEvent> {

    private StackPane layout;

    public ResetController(StackPane layout) {
        this.layout = layout;
    }

    @Override
    public void handle(ActionEvent event) {
        layout.getChildren().clear();

        FunctionGraph functionGraph2= new FunctionGraph();
        layout.getChildren().add(functionGraph2);
    }
}
