package lab6;

import net.objecthunter.exp4j.Expression;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunctionLagrangeForm;

public class Utility {

    public static PolynomialFunctionLagrangeForm getLagrangeForm(Expression e) {
        double[] x = {-8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8};
        double[] y = new double[x.length];
        for (int i = 0; i < y.length; i++) {
            e.setVariable("x", x[i]);
            y[i] = e.evaluate();
        }
        return new PolynomialFunctionLagrangeForm(x, y);
    }
}
