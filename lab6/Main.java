package lab6;

import lab6.controller.DrawController;
import lab6.controller.LoadController;
import lab6.controller.ResetController;
import lab6.controller.SaveController;
import lab6.view.FunctionGraph;

import javafx.application.Application;
import javafx.geometry.Insets;

import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.text.*;
import javafx.stage.*;

public class Main extends Application {

    private StackPane layout = new StackPane();

    @Override
    public void start(final Stage stage) {


        FunctionGraph functionGraph = new FunctionGraph();

        layout.setPadding(new Insets(10));
        layout.setStyle("-fx-background-color: rgb(255, 255, 255);");
        layout.getChildren().add(functionGraph);


        Text functionLabel = new Text();
        functionLabel.setText("Function: ");
        TextField functionTextField = new TextField();

        Text colorLabel = new Text("Color :");
        ColorPicker colorPicker = new ColorPicker();
        colorPicker.setValue(Color.RED);

        Text strokeLabel = new Text();
        strokeLabel.setText("Stroke: ");
        TextField strokeField = new TextField();


        Button drawFunction = new Button();
        drawFunction.setText("Draw Function");
        drawFunction.setOnAction(new DrawController(functionTextField, colorPicker, layout, strokeField));

        Button reset = new Button();
        reset.setText("Reset");
        reset.setOnAction(new ResetController(layout));

        Button save = new Button();
        save.setText("Save");
        save.setOnAction(new SaveController(stage, layout));

        Button load = new Button();
        load.setText("Load");
        load.setOnAction(new LoadController(stage, layout));

        GridPane grid = new GridPane();
        grid.setVgap(4);
        grid.setHgap(10);
        grid.setPadding(new Insets(5, 5, 5, 5));

        grid.add(functionLabel, 0, 1);
        grid.add(functionTextField, 1, 1);

        grid.add(drawFunction, 0, 2);
        grid.add(reset, 1, 2);
        grid.add(save, 2, 2);
        grid.add(load, 3, 2);


        grid.add(colorLabel, 0, 3);
        grid.add(colorPicker, 1, 3);

        grid.add(strokeLabel, 0, 4);
        grid.add(strokeField, 1, 4);

        grid.add(layout, 4, 2);


        stage.setTitle("Function Graph");
        stage.setScene(new Scene(grid, 900, 450));
        stage.show();
    }
}