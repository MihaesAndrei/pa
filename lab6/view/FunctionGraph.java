package lab6.view;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import lab6.Utility;
import net.objecthunter.exp4j.Expression;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunctionLagrangeForm;

public class FunctionGraph extends Pane {

    private double xMin = -8;
    private double xMax = 8;
    private double precision = 0.1;
    private Axes axes;

    public FunctionGraph() {

        axes = new Axes();


        setMinSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);
        setPrefSize(axes.getPrefWidth(), axes.getPrefHeight());
        setMaxSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);

        getChildren().setAll(axes);

    }

    public FunctionGraph(Expression expression, Color color, int strokeWidth) {

        axes = new Axes();

        Path path = new Path();
        path.setStroke(color);
        path.setStrokeWidth(strokeWidth);

        path.setClip(new Rectangle(0, 0, axes.getPrefWidth(), axes.getPrefHeight()));


        PolynomialFunctionLagrangeForm lagrangeForm = Utility.getLagrangeForm(expression);

        double x = xMin;
        double y = lagrangeForm.value(x);

        path.getElements().add(new MoveTo(translateX(x, axes), translateY(y, axes)));

        x += precision;
        while (x < xMax) {
            y = lagrangeForm.value(x);
            path.getElements().add(new LineTo(translateX(x, axes), translateY(y, axes)));

            x += precision;
        }

        setMinSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);
        setPrefSize(axes.getPrefWidth(), axes.getPrefHeight());
        setMaxSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);

        getChildren().setAll(axes, path);

    }

    private double translateX(double x, Axes axes) {
        double tx = axes.getPrefWidth() / 2;
        double sx = axes.getPrefWidth() / (axes.getXAxis().getUpperBound() - axes.getXAxis().getLowerBound());

        return x * sx + tx;
    }

    private double translateY(double y, Axes axes) {
        double ty = axes.getPrefHeight() / 2;
        double sy = axes.getPrefHeight() / (axes.getYAxis().getUpperBound() - axes.getYAxis().getLowerBound());

        return -y * sy + ty;
    }
}
