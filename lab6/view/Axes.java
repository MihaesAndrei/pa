package lab6.view;

import javafx.beans.binding.Bindings;
import javafx.geometry.Side;
import javafx.scene.chart.NumberAxis;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class Axes extends Pane {

    private NumberAxis xAxis;
    private NumberAxis yAxis;

    private int width = 400;
    private int height = 300;
    private double xLow = -8;
    private double xHi = 8;
    private double xTickUnit = 1;
    private double yLow = -6;
    private double yHi = 6;
    private  double yTickUnit = 2;

    public Axes() {
        setMinSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);
        setPrefSize(width, height);
        setMaxSize(Pane.USE_PREF_SIZE, Pane.USE_PREF_SIZE);

        xAxis = new NumberAxis(xLow, xHi, xTickUnit);
        xAxis.setSide(Side.BOTTOM);
        xAxis.setMinorTickVisible(false);
        xAxis.setPrefWidth(width);
        xAxis.setLayoutY(height / 2);

        yAxis = new NumberAxis(yLow, yHi, yTickUnit);
        yAxis.setSide(Side.LEFT);
        yAxis.setMinorTickVisible(false);
        yAxis.setPrefHeight(height);
        yAxis.layoutXProperty().bind(
                Bindings.subtract(
                        (width / 2) + 1,
                        yAxis.widthProperty()
                )
        );

        Text yMark = new Text(width / 2 + 6, 5, "y");
        Text xMark = new Text(width , height/2 - 5, "x");


        getChildren().setAll(xAxis, yAxis, xMark, yMark);
    }

    public NumberAxis getXAxis() {
        return xAxis;
    }

    public NumberAxis getYAxis() {
        return yAxis;
    }
}