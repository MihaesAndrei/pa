package lab2;

import java.util.*;

public class Student extends Person {

    private List<Project> preferredProjects = new ArrayList<Project>();
    private Project subscribedProject;

    public Student(String name) {
        super(name);
    }

    public void setPreferredProjects(Project... projects) {
        for (Project project : projects) {
            preferredProjects.add(project);
        }
    }

    public List<Project> getPreferredProjects() {
        return preferredProjects;
    }

    public String toString() {
        return getName();
    }

    public boolean isFree() { /* a student is free if it has no project */
        return getSubscribedProject() == null && !getPreferredProjects().isEmpty();
    }

    public Project getMostPreferredProject() {
        return preferredProjects.get(0);
    }

    public Project getSubscribedProject() {
        return subscribedProject;
    }

    public void setSubscribedProject(Project subscribedProject) {
        this.subscribedProject = subscribedProject;
    }

    public void subscribeProject(Project p) {
        preferredProjects.remove(p);
        setSubscribedProject(p);
        p.subscribeStudent(this);
        p.getLecturer().subscribeStudent(this);
    }

    public void unsubscribeProject(Project p) {
        subscribedProject = null;
        p.getLecturer().unsubscribeStudent(this);
        p.unsubscribeStudent(this);

    }
}
