package lab2;

import java.util.ArrayList;
import java.util.List;

public class Project {

    private String name;
    private int capacity;
    private Lecturer lecturer;
    public List<Student> subscribedStudents = new ArrayList<Student>();

    public Project(String name, int capacity, Lecturer lecturer) {
        setName(name);
        setCapacity(capacity);
        setLecturer(lecturer);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    public String toString() {
        return name;
    }

    public boolean isOverSubscribed() {
        return subscribedStudents.size() > getCapacity();
    }

    public void subscribeStudent(Student s) {
        subscribedStudents.add(s);
    }
    public void unsubscribeStudent(Student s) { subscribedStudents.remove(s); }

    public Student getWorstStudentAssigned() {
        List<Student> preferredStudents = lecturer.getPreferredStudents();

        Student worstStudent = subscribedStudents.get(0);
        for(Student subscribedStudent : subscribedStudents) {
            if(!preferredStudents.contains(subscribedStudent)) return subscribedStudent;
        }
        for(int i = 1; i < subscribedStudents.size(); i++) {
            Student subscribedStudent = subscribedStudents.get(i);
            if(preferredStudents.indexOf(worstStudent) < preferredStudents.indexOf(subscribedStudent)) {
                worstStudent = subscribedStudent;
            }
        }
        return worstStudent;
//
//        int index;
//        Student returnStudent;
//
//        List<Student> preferredStudents = lecturer.getPreferredStudents();
//
//        for(int i = (preferredStudents.size() - 1); i >= 0; i--){
//            Student student = preferredStudents.get(i);
//            index = subscribedStudents.indexOf(student);
//
//            // when we find a student, we remove it (as it's the least found)
//            if(index != -1){
//                returnStudent = subscribedStudents.get(index);
//
//                return returnStudent;
//            }
//        }
//
//        // if the student is not found in the preferences, we remove the first
//        returnStudent = subscribedStudents.get(0);
//        return returnStudent;
    }

}
