package lab2;

public class Main {

    private static Student[] s;
    private static Lecturer[] l;
    private static Project[] p;

    public static void main(String[] args) {

        createExample();

        s = new Student[3];
        s[0] = new Student("S1");
        s[1] = new Student("S2");
        s[2] = new Student("S3");

        l = new Lecturer[2];
        l[0] = new Lecturer("L1", 2);
        l[1] = new Lecturer("L2", 2);

        p = new Project[3];
        p[0] = new Project("P1", 1, l[0]);
        p[1] = new Project("P2", 1, l[0]);
        p[2] = new Project("P3", 1, l[1]);

        s[0].setPreferredProjects(p[0], p[1], p[2]);
        s[1].setPreferredProjects(p[1], p[2]);
        s[2].setPreferredProjects(p[0], p[1], p[2]);

        l[0].setPreferredStudents(s[1], s[2]);
        l[1].setPreferredStudents(s[0]);

        Problem problem = new Problem(s, l, p);
        System.out.println(problem);

        Solver solver = new Solver(problem);
        solver.showResult();

    }


    private static void createExample() {
        Student[] s = new Student[7];
        Lecturer[] l = new Lecturer[3];
        Project[] p = new Project[8];

        initializeExample(s, l ,p);

        Problem example = new Problem(s, l, p);
        System.out.print(example);

        Solver solver = new Solver(example);
        solver.showResult();
    }

    private static void initializeExample(Student[] s, Lecturer[] l, Project[] p) {
        s[0] = new Student("S1");
        s[1] = new Student("S2");
        s[2] = new Student("S3");
        s[3] = new Student("S4");
        s[4] = new Student("S5");
        s[5] = new Student("S6");
        s[6] = new Student("S7");

        l[0] = new Lecturer("L1", 3);
        l[1] = new Lecturer("L2", 2);
        l[2] = new Lecturer("L3", 2);

        p[0] = new Project("P1", 2, l[0]);
        p[1] = new Project("P2", 1, l[0]);
        p[2] = new Project("P3", 1, l[0]);
        p[3] = new Project("P4", 1, l[1]);
        p[4] = new Project("P5", 1, l[1]);
        p[5] = new Project("P6", 1, l[1]);
        p[6] = new Project("P7", 1, l[2]);
        p[7] = new Project("P8", 1, l[2]);

        s[0].setPreferredProjects(p[0], p[6]);
        s[1].setPreferredProjects(p[0], p[1], p[2], p[3], p[4], p[5]);
        s[2].setPreferredProjects(p[1], p[0], p[3]);
        s[3].setPreferredProjects(p[1]);
        s[4].setPreferredProjects(p[0], p[1], p[2], p[3]);
        s[5].setPreferredProjects(p[1], p[2], p[3], p[4], p[5]);
        s[6].setPreferredProjects(p[4], p[2], p[7]);

        l[0].setPreferredStudents(s[6], s[3], s[0], s[2], s[1], s[4], s[5]);
        l[1].setPreferredStudents(s[2], s[1], s[5], s[6], s[4]);
        l[2].setPreferredStudents(s[0], s[6]);
    }
}
