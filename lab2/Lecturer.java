package lab2;

import java.util.*;

public class Lecturer extends Person {

    private int capacity;
    private List<Student> preferredStudents = new ArrayList<Student>();
    private List<Student> subscribedStudents = new ArrayList<Student>();

    public Lecturer(String name, int capacity) {
        super(name);
        setCapacity(capacity);
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setPreferredStudents(Student... students) {
        for (Student student : students) {
            preferredStudents.add(student);
        }
    }

    public List<Student> getPreferredStudents() {
        return preferredStudents;
    }

    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof Lecturer)) return false;
        Lecturer otherLecturer = (Lecturer) other;
        return otherLecturer.getName().equals(getName());

    }

    public String toString() {
        return getName();
    }

    public boolean isFree() { /*  a lecturer is free if its capacity has not been reached */
        return subscribedStudents.size() < getCapacity();
    }

    public boolean isOverSubscribed() {
        if (subscribedStudents.size() > getCapacity()) return true;
        else return false;
    }

    public void subscribeStudent(Student s) {
        subscribedStudents.add(s);
    }

    public void unsubscribeStudent(Student s) {
        subscribedStudents.remove(s);
    }

    public Student getSubscribedStudent(int index) {
        return subscribedStudents.get(index);
    }

    public Student getWorstStudentAssigned() {

        return subscribedStudents.get(subscribedStudents.size() - 1);
    }
}
