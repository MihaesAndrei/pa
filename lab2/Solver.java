package lab2;

public class Solver {

    Problem problem;

    public Solver(Problem p) {

        this.problem = p;

        Student[] students = p.getStudents();

        Student student = someStudentIsFree(students);
        while(student != null && !student.getPreferredProjects().isEmpty()) {

            Project project = student.getMostPreferredProject();
            Lecturer lecturer = project.getLecturer();

            student.subscribeProject(project); // provisionally assign si to pj ;

            if(project.isOverSubscribed()) {
                Student worstStudent =  project.getWorstStudentAssigned();
                worstStudent.unsubscribeProject(project);
            }
            else if(lecturer.isOverSubscribed()){
                Student worstStudent = lecturer.getWorstStudentAssigned();
                Project projectAssigned = worstStudent.getSubscribedProject();
                worstStudent.unsubscribeProject(projectAssigned);
            }
            student = someStudentIsFree(students);
        }

    }

    private Student someStudentIsFree(Student[] students) {
        for(Student student : students) {
            if(student.isFree()) return student;
        }
        return null;
    }

    public void showResult() {
        Student[] students = problem.getStudents();

        System.out.println("Student-Project Allocation");
        for (Student student : students) {
            System.out.println(student + ": " + student.getSubscribedProject());
        }
        System.out.println();
    }
}
