package lab2;

import java.util.*;

public class Problem {
    private Student[] students;
    private Lecturer[] lecturers;
    private Project[] projects;

    public Problem(Student[] students, Lecturer[] lecturers, Project[] projects) {
        this.setStudents(students);
        this.setLecturers(lecturers);
        this.setProjects(projects);
    }

    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append("Student preferences\n");
        for (Student student : getStudents()) {

            StringBuilder studentPrefsSB = getStudPref(student);
            result.append(studentPrefsSB);
        }

        result.append("\nLecturer preferences\n");
        for (Lecturer lecturer : getLecturers()) {

            StringBuilder lecturerPrefsSB = getLectPref(lecturer);
            result.append(lecturerPrefsSB);
        }

        result.append("\nAvailable projects\n");
        for (Lecturer currentLecturer : getLecturers()) {
            StringBuilder lectOffers = getLectOffers(currentLecturer);
            result.append(lectOffers);
        }
        result.append('\n');
        return result.toString();
    }

    private StringBuilder getLectOffers(Lecturer currentLecturer) {
        StringBuilder lectOffers = new StringBuilder();
        lectOffers.append(currentLecturer);
        lectOffers.append(" offers ");

        ArrayList<Project> projecectsOffered = new ArrayList<Project>();
        for (int i = 0; i < this.getProjects().length; i++) {
            Lecturer otherLecturer = this.getProjects()[i].getLecturer();
            if (otherLecturer.equals(currentLecturer))
                projecectsOffered.add(this.getProjects()[i]);
        }
        for (int i = 0; i < projecectsOffered.size(); i++) {
            lectOffers.append(projecectsOffered.get(i));
            if (i < projecectsOffered.size() - 1) lectOffers.append(", ");
        }
        lectOffers.append('\n');
        return lectOffers;
    }

    private StringBuilder getLectPref(Lecturer lecturer) {
        StringBuilder lecturerPrefsSB = new StringBuilder();
        lecturerPrefsSB.append(lecturer);
        lecturerPrefsSB.append(": ");

        List<Student> preferredStudents = lecturer.getPreferredStudents();
        for (int i = 0; i < preferredStudents.size(); i++) {
            lecturerPrefsSB.append(preferredStudents.get(i).getName());
            if (i != preferredStudents.size() - 1) lecturerPrefsSB.append(" ");
        }
        lecturerPrefsSB.append('\n');
        return lecturerPrefsSB;
    }

    private StringBuilder getStudPref(Student student) {
        StringBuilder studentPrefsSB = new StringBuilder();
        studentPrefsSB.append(student);
        studentPrefsSB.append(": ");

        List<Project> preferredProjects = student.getPreferredProjects();
        for (int i = 0; i < preferredProjects.size(); i++) {
            studentPrefsSB.append(preferredProjects.get(i));
            if (i != preferredProjects.size() - 1) studentPrefsSB.append(" ");
        }
        studentPrefsSB.append('\n');
        return studentPrefsSB;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    public Lecturer[] getLecturers() {
        return lecturers;
    }

    public void setLecturers(Lecturer[] lecturers) {
        this.lecturers = lecturers;
    }

    public Project[] getProjects() {
        return projects;
    }

    public void setProjects(Project[] projects) {
        this.projects = projects;
    }
}
