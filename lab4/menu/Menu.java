package lab4.menu;

import lab4.commands.Command;

import java.io.IOException;
import java.util.List;

public interface Menu {

    void addCommand(Command command);

    void addCommands(List<Command> commands);

    void run() throws IOException;
}
