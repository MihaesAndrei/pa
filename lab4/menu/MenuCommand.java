package lab4.menu;

import lab4.commands.*;
import lab4.commands.exceptions.UsageArgsException;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class MenuCommand implements Menu {

    private List<Command> commandList;
    private BufferedReader bufferedReader;
    private Path currentDir;

    public MenuCommand() {
        commandList = new ArrayList<>();
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        initializeCurrentDir(); /* relative to the application */
    }

    private void initializeCurrentDir() {
         /* setting current directory relative to the application */
        currentDir = Paths.get("");
        /* this gets the correct path, but getNameCount() will always be 1 */

        StringBuilder sb = new StringBuilder(currentDir.toAbsolutePath().toString());
        currentDir = Paths.get(sb.toString());
        /* now currentDir.getNameCount() has the right value */
    }

    public Path getCurrentDir() { return this.currentDir; }

    public void setCurrentDir(Path path) { this.currentDir = path; }

    public void addCommand(Command command) {
        commandList.add(command);
        command.addMenu(this);
    }

    public void addCommands(List<Command> commands) {
        for (Command command : commands) {
            addCommand(command);
        }
    }

    public void printAvailableCommands() {
        System.out.println("Available commands:");
        commandList.forEach(System.out::println);
    }

    public List<Command> getCommandList() {
        return this.commandList;
    }

    public void run() throws IOException {
        while (true) {
            /* read command from Input */
            System.out.print(getCurrentDir() + " > ");
            String lineCommand = bufferedReader.readLine();

            /* extract commandName and arguments from input */
            StringCommand stringCommand = new StringCommand(lineCommand).invoke();
            String commandName = stringCommand.getCommandName();
            List<String> args = stringCommand.getArgs();

            executeCommand(commandName, args);
            System.out.println();
        }
    }

    private void executeCommand(String commandName, List<String> args) {
        boolean found = false;

        for (Command command : commandList) {
            if (command.getCommandName().equals(commandName)) {
                try {
                    command.execute(args);
                } catch (UsageArgsException e) {
                    System.out.println("Usage: " + e.getUsage());
                }
                found = true;
            }
        }
        if (!found) {
            System.out.println("'" + commandName + "' " + "is not recognized as a command");
        }
    }

    private class StringCommand {
        private String lineCommand;
        private String commandName;
        private List<String> args;

        public StringCommand(String lineCommand) {
            this.lineCommand = lineCommand;
        }

        public String getCommandName() {
            return commandName;
        }

        public List<String> getArgs() {
            return args;
        }

        public StringCommand invoke() {
            StringTokenizer st = new StringTokenizer(lineCommand);
            args = new ArrayList<>();
            boolean flag = false;
            while (st.hasMoreTokens()) {

                if (!flag) {
                    commandName = st.nextToken();
                    flag = true;
                } else {
                    args.add(st.nextToken());
                }
            }
            return this;
        }
    }
}
