package lab4;

import lab4.commands.factory.CommandFactory;
import lab4.menu.*;

import java.io.IOException;

public class Main {
    public static void main(String args[]) {

        Menu menu = new MenuCommand();
        menu.addCommands(CommandFactory.getAudioCommands());
        menu.addCommands(CommandFactory.getBasicCommands());

        try {
            menu.run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
