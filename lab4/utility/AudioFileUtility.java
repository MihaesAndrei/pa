package lab4.utility;

import org.apache.sis.util.StringBuilders;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.util.*;


public class AudioFileUtility {

    private static final String PATTERN = "*.{mp3,flac,wav}"; /* regular expression */

    public static List<Path> findFiles(Path path, int depth) {
        AudioFinder audioFinder = new AudioFinder(PATTERN);
        EnumSet<FileVisitOption> opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS); /*  symbolic links should be followed. */
        try {
            Files.walkFileTree(path, opts, depth, audioFinder); /* depth = 1 (find in current directory) */
        } catch (IOException e) {
            e.printStackTrace();
        }
        return audioFinder.getFiles();
    }

    public static void showFiles(List<Path> files) {
        for (Path file : files) {
            System.out.println(file.toFile().getName());
        }
        System.out.println("Number of files: " + files.size());
    }

    public static Metadata getMetadata(File audioFile) {

        Parser parser = new AutoDetectParser();
        BodyContentHandler handler = new BodyContentHandler();
        Metadata metadata = new Metadata();
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(audioFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ParseContext context = new ParseContext();

        try {
            parser.parse(fileInputStream, handler, metadata, context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (TikaException e) {
            e.printStackTrace();
        }
        return metadata;
    }

    public static URI getURI(File file) {
        Metadata metadata = getMetadata(file);
        StringBuilder sb = new StringBuilder();
        sb.append("https://www.google.ro/search?q=").append(metadata.get("xmpDM:artist").replace(" ", "%20")).
                append("+").append(metadata.get("title").replace(" ", "%20"));
        URI uri = null;
        try {
            uri = new URI(sb.toString());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return uri;
    }


    public static String showMetadata(Metadata metadata) {
        StringBuilder sb = new StringBuilder("");
        sb.append("Audio info: \n\n");
        String[] metadataNames = {"title", "xmpDM:artist", "xmpDM:album", "xmpDM:releaseDate"};
        sb.append("Title: ").append(metadata.get(metadataNames[0])).append("\n");
        sb.append("Artist: ").append(metadata.get(metadataNames[1])).append("\n");
        sb.append("Album: ").append(metadata.get(metadataNames[2])).append("\n");
        sb.append("Year: ").append(metadata.get(metadataNames[3]));
        return sb.toString();
    }

    public static List<Path> filterFiles(List<Path> files, String metadataName, String value) {

        List<Path> result = new ArrayList<>(files);

        for(Iterator<Path> iterator = result.iterator(); iterator.hasNext();) {
            File audioFile = new File(iterator.next().toAbsolutePath().toString());
            String metadata = getMetadata(audioFile).get(metadataName);
            if (!metadata.equals(value) && !metadata.equals("null")) iterator.remove();
        }
        return result;

    }
}
