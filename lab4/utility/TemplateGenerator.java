package lab4.utility;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class TemplateGenerator {

    public TemplateGenerator(int count) {

        String header = "<html>\n" +
                "<head>\n" +
                "  <title>FavList</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "  <h2>Favorites list:</h2>";
        String footer = "</body>\n" +
                "</html>";
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("test.ftl", "UTF-8");
            writer.write(header);
            for(int i = 0; i < count; i++) {
               writer.write("\n<p>Path: ${path" + i + "}</p>\n" +
                       "  \t<ul>\n" +
                       "  \t\t<li>Artist: ${artist" + i + "}</li>\n" +
                       "  \t\t<li>Title: ${title" + i + "}</li>\n" +
                       "  \t\t<li>Album: ${album" + i + "}</li>\n" +
                       "  \t\t<li>Year: ${year" + i + "}</li>\n" +
                       "  \t</ul>");
            }
            writer.write(footer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            if(writer != null)  writer.close();
        }
    }
}
