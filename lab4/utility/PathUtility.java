package lab4.utility;

import java.nio.file.*;

public class PathUtility {

    public static Path changeDirectoryTo(String arg, Path currentPath) {

        Path path = Paths.get(arg + "\\");
        if (path.isAbsolute()) return path;
        else {
            if (arg.equals("..")) return PathUtility.goBack(currentPath);
            else return PathUtility.goForward(arg, currentPath);
        }
    }

    public static Path goForward(String arg, Path currentPath) {

        Path temp = relativeToAbsolute(arg, currentPath);

        /* check if path exists */
        boolean exists = Files.exists(temp, LinkOption.NOFOLLOW_LINKS);

        if (!exists) {
            return null;
        } else if (!arg.equals(".")) /* modify dir only if args is not equal to "." */
            currentPath = temp;
        return currentPath;
    }

    public static Path relativeToAbsolute(String arg, Path currentPath) {
        StringBuilder sb = new StringBuilder(currentPath.toAbsolutePath().toString());
        sb.append("\\").append(arg);

        return Paths.get(sb.toString());
    }

    public static Path goBack(Path currentPath) {
        if (currentPath.getNameCount() > 1) {
            StringBuilder sb = new StringBuilder(currentPath.toAbsolutePath().toString());
            sb.delete(sb.lastIndexOf("\\"), sb.length());
            return Paths.get(sb.toString());
        } else { /* set root folder with '\' appended. ( example -> D:\ ) */
            StringBuilder sb = new StringBuilder(currentPath.toAbsolutePath().toString());
            sb.delete(sb.lastIndexOf("\\"), sb.length());
            sb.append("\\");
            return Paths.get(sb.toString());
        }
    }
}
