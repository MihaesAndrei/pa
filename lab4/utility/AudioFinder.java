package lab4.utility;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

import static java.nio.file.FileVisitResult.CONTINUE;

public class AudioFinder extends SimpleFileVisitor<Path> {

    private final PathMatcher matcher;
    private List<Path> files = new ArrayList<>();

    public AudioFinder(String pattern) {
        matcher = FileSystems.getDefault().getPathMatcher("glob:" + pattern);
    }

    /* Compares the glob pattern against the file or directory name. */
    public void find(Path file) {
        Path name = file.getFileName();
        if (name != null && matcher.matches(name)) {
            files.add(file);
        }
    }

    /* Invoke the pattern matching method on each file. */
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        find(file);
        return CONTINUE;
    }

    /* Invoke the pattern matching method on each directory. */
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        find(dir);
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        System.err.println(exc);
        return CONTINUE;
    }

    public List<Path> getFiles() {
        return this.files;
    }
}
