package lab4.utility;

import org.apache.tika.metadata.Metadata;

import java.io.*;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FavoritesList {

    /* Use List of Strings because Path cant be serialized */
    public static List<String> myFavoritesList = getStoredList();

    public static boolean addFile(Path file) {
        if(myFavoritesList.contains(file.toString()))
            return false;
        else {
            myFavoritesList.add(file.toString());
            storeList();
            return true;
        }
    }

    public static String getPath(int index) {
        return myFavoritesList.get(index);
    }

    public static String getTitle(int index) {
        File audioFile = new File(myFavoritesList.get(index));
        Metadata metaData = AudioFileUtility.getMetadata(audioFile);
        return metaData.get("title");
    }

    public static String getArtist(int index) {
        File audioFile = new File(myFavoritesList.get(index));
        Metadata metaData = AudioFileUtility.getMetadata(audioFile);
        return metaData.get("xmpDM:artist");
    }

    public static String getAlbum(int index) {
        File audioFile = new File(myFavoritesList.get(index));
        Metadata metaData = AudioFileUtility.getMetadata(audioFile);
        return metaData.get("xmpDM:album");
    }

    public static String getYear(int index) {
        File audioFile = new File(myFavoritesList.get(index));
        Metadata metaData = AudioFileUtility.getMetadata(audioFile);
        return metaData.get("xmpDM:releaseDate");
    }



    private static void storeList() {
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("favList.ser"))) {
            out.writeObject(myFavoritesList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<String> getStoredList() {
        List<String> result = new ArrayList<>();

        try (ObjectInputStream in = new ObjectInputStream(new FileInputStream("favList.ser"))) {
            result = (List<String>) in.readObject();

            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("Empty Fav List");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void showFavList() {
        System.out.println("My Fav List");
        for (String path : FavoritesList.myFavoritesList) {
            System.out.println(path);
        }
    }
}
