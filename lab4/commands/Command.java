package lab4.commands;

import lab4.commands.exceptions.UsageArgsException;
import lab4.menu.*;

import java.util.List;

public abstract class Command {

    protected String COMMAND ="";
    protected String INFO = "";
    protected String USAGE = "";
    protected MenuCommand menuCommand;

    public String getCommandName() { return COMMAND; }

    public String getInfo() { return INFO; }

    public String getManual() {
        StringBuilder result = new StringBuilder();
        result.append("Command: ").append(COMMAND).append("\n").append("Info: ").append(INFO).append("\n").append("Usage: ").append(USAGE);
        return result.toString();
    }

    public void addMenu(MenuCommand menu) { this.menuCommand = menu; }

    @Override
    public String toString() { return "<" + getCommandName() + "> : " + getInfo(); }

    public abstract void execute(List<String> args) throws UsageArgsException;
}
