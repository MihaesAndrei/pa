package lab4.commands.basic;

import lab4.commands.Command;
import lab4.commands.exceptions.UsageArgsException;

import java.util.List;

public class ManCommand extends Command {

    public ManCommand() {
        this.COMMAND = "man";
        this.INFO = "generate a man page (short for manual page) for an existent command";
        this.USAGE = "man [command name]";
    }

    @Override
    public void execute(List<String> args) throws UsageArgsException {
        if(args.size() != 1) throw new UsageArgsException(USAGE);
        else {
            List<Command> commandList = this.menuCommand.getCommandList();
            for (Command command : commandList) {
                if (command.getCommandName().equals(args.get(0))) {
                    System.out.println(command.getManual());
                    return;
                }
            }
        }
        System.out.println("Command not found");
    }
}
