package lab4.commands.basic;

import lab4.commands.Command;
import lab4.commands.exceptions.UsageArgsException;

import java.util.List;

public class HelpCommand extends Command {

    public HelpCommand() {
        this.COMMAND = "help";
        this.INFO = "shows available commands";
        this.USAGE = "help";
    }

    @Override
    public void execute(List<String> args) throws UsageArgsException {
        this.menuCommand.printAvailableCommands();
    }
}
