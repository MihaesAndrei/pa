package lab4.commands.basic;

import lab4.commands.Command;
import lab4.commands.exceptions.UsageArgsException;

import java.util.List;

public class QuitCommand extends Command {

    public QuitCommand() {
        this.COMMAND = "quit";
        this.INFO = "Quit Audio Library Manager";
        this.USAGE = "quit";
    }

    @Override
    public void execute(List<String> args) throws UsageArgsException {
        if(args.size() > 0) throw new UsageArgsException(USAGE);
        else {
            System.exit(0);
        }
    }
}
