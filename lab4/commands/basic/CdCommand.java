package lab4.commands.basic;

import lab4.commands.Command;
import lab4.commands.exceptions.UsageArgsException;
import lab4.utility.PathUtility;

import java.nio.file.*;
import java.util.*;

public class CdCommand extends Command {

    public CdCommand() {
        this.COMMAND = "cd";
        this.INFO = "setting the current directory";
        this.USAGE = "cd [directory]";
    }

    @Override
    public void execute(List<String> args) throws UsageArgsException {
        switch (args.size()) {

            /* Stay in same directory, so no action */
            case 0:
                break;

            case 1:
                Path path = PathUtility.changeDirectoryTo(args.get(0), getCurrentDir());
                if(path == null) { System.out.println("The system cannot find the path specified."); return ; }
                else setCurrentDir(path);
                break;

            default:
                throw new UsageArgsException(USAGE);
        }
    }

    private Path getCurrentDir() { return this.menuCommand.getCurrentDir(); }

    private void setCurrentDir(Path path) { this.menuCommand.setCurrentDir(path); }
}

