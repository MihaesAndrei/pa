package lab4.commands.exceptions;

public class UsageArgsException extends Exception {

    private String usage;

    public UsageArgsException(String usage) {
        this.usage = usage;
    }

    public String getUsage() {
        return this.usage;
    }

}
