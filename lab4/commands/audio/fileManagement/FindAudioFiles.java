package lab4.commands.audio.fileManagement;

import lab4.commands.Command;
import lab4.commands.exceptions.UsageArgsException;
import lab4.utility.AudioFileUtility;

import java.nio.file.Path;
import java.util.List;

public class FindAudioFiles extends Command {

    public FindAudioFiles() {
        this.COMMAND = "find";
        this.INFO = "search for a song having a particular title, or artist, or album, or year";
        this.USAGE = "find [metadata name] [value]";
    }

    @Override
    public void execute(List<String> args) throws UsageArgsException {
        Path path;
        if(args.size() >= 2) {

            /* concatenate args */
            StringBuilder sb = new StringBuilder(args.get(1));
            for(int i = 2; i < args.size(); i++) {
                sb.append(" ").append(args.get(i));
            }
            args.set(1, sb.toString());

            path = this.menuCommand.getCurrentDir();

            List<Path> files = AudioFileUtility.findFiles(path, Integer.MAX_VALUE);
            String metadataName = "";
            switch (args.get(0)) {
                case "title":
                    metadataName = "title";
                    break;
                case "artist":
                    metadataName = "xmpDM:artist";
                    break;
                case "album":
                    metadataName = "xmpDM:album";
                    break;
                case "year":
                    metadataName = "xmpDM:releaseDate";
                    break;
                default:
                    System.out.println("Invalid Filter!");
                    return;

            }
            AudioFileUtility.showFiles(AudioFileUtility.filterFiles(files, metadataName, args.get(1)));
        } else {
            throw new UsageArgsException(USAGE);
        }
    }
}
