package lab4.commands.audio.fileManagement;

import lab4.commands.Command;
import lab4.commands.exceptions.UsageArgsException;
import lab4.utility.*;

import java.nio.file.Path;
import java.util.List;

public class ListCommand extends Command {

    public ListCommand() {
        this.COMMAND = "list";
        this.INFO = "listing of the audio files in a specific directory or in the current directory";
        this.USAGE = "list [directory]";
    }

    @Override
    public void execute(List<String> args) throws UsageArgsException {

        Path path;
        switch (args.size()) {

            /* find files in current directory */
            case 0:
                path = this.menuCommand.getCurrentDir();
                /* depth = 1 (find in current directory) */
                AudioFileUtility.showFiles(AudioFileUtility.findFiles(path, 1));
                break;

            /* find files in the path argument */
            case 1:
                path = PathUtility.changeDirectoryTo(args.get(0), this.menuCommand.getCurrentDir());
                if(path == null) {
                    System.out.println("The system cannot find the path specified.");
                } else {
                    AudioFileUtility.showFiles(AudioFileUtility.findFiles(path, 1));
                }
                break;

            default:
                throw new UsageArgsException(USAGE);
        }
    }
}
