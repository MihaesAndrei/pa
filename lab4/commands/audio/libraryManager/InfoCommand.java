package lab4.commands.audio.libraryManager;

import lab4.commands.Command;
import lab4.commands.exceptions.UsageArgsException;
import lab4.utility.*;
import org.apache.tika.metadata.Metadata;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class InfoCommand extends Command {

    public InfoCommand() {
        this.COMMAND = "info";
        this.INFO = "display the metadata of a specific file: title, artist, album, year";
        this.USAGE = "info [file path]";
    }

    @Override
    public void execute(List<String> args) throws UsageArgsException {
        switch (args.size()) {

            case 0:
                throw new UsageArgsException(USAGE);

            case 1:
                Path filePath = PathUtility.changeDirectoryTo(args.get(0), this.menuCommand.getCurrentDir());

                if(filePath == null) {
                    System.out.println("The system cannot find the path specified.");
                    break;
                }

                File audioFile = new File(filePath.toAbsolutePath().toString());
                if(audioFile.exists()) {
                    System.out.println(AudioFileUtility.showMetadata(AudioFileUtility.getMetadata(audioFile)));
                    break;
                }

            default:
                System.out.println("Too many arguments...");
                break;
        }
    }

    public String execute(File file) {
        return AudioFileUtility.showMetadata(AudioFileUtility.getMetadata(file));
    }

    public static Object[][] getMetadataTable(File file) {
        List<Path> pathList = AudioFileUtility.findFiles(Paths.get(file.toString()), 1);

        Object[][] data = new Object[pathList.size()][5];
        int index = 0;
        for (Path path : pathList) {
            File f = path.toFile();
            Metadata metadata = AudioFileUtility.getMetadata(f);
            data[index][0] = f.getName();
            data[index][1] = metadata.get("title");
            data[index][2] = metadata.get("xmpDM:artist");
            data[index][3] = metadata.get("xmpDM:album");
            data[index][4] = metadata.get("xmpDM:releaseDate");
            index++;
        }
        return data;
    }
}
