package lab4.commands.audio.libraryManager;

import java.util.List;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import lab4.commands.Command;
import lab4.commands.exceptions.UsageArgsException;
import lab4.utility.FavoritesList;
import lab4.utility.TemplateGenerator;

import java.util.*;
import java.io.*;

public class ReportCommand extends Command {

    private Configuration cfg = new Configuration();

    public ReportCommand() {
        this.COMMAND = "report";
        this.INFO = "create a HTML report containing your favorite songs";
        this.USAGE = "report";

        /* Create and adjust the configuration singleton */
        try {
            cfg.setDirectoryForTemplateLoading(new File("."));
        } catch (IOException e) {
            e.printStackTrace();
        }
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    }

    @Override
    public void execute(List<String> args) throws UsageArgsException {

        if(FavoritesList.myFavoritesList.size() == 0) {
            System.out.println("You don't have any favorite songs to store.");
        } else {
            if (args.size() > 0) throw new UsageArgsException(USAGE);
        /* Create a data-model */
            Map root = new HashMap();

            TemplateGenerator templateGenerator = new TemplateGenerator(FavoritesList.myFavoritesList.size());
            for(int i = 0; i < FavoritesList.myFavoritesList.size(); i++) {

                root.put("path" + i, FavoritesList.getPath(i));
                root.put("artist" + i, FavoritesList.getArtist(i));
                root.put("title" + i, FavoritesList.getTitle(i));
                root.put("album" + i, FavoritesList.getAlbum(i));
                root.put("year" + i, FavoritesList.getYear(i));
            }


            try {
                Template temp = cfg.getTemplate("test.ftl");

            /* Merge data-model with template */
                Writer out = new PrintWriter("test.html", "UTF-8");
                temp.process(root, out);
                System.out.println("Report created successfully...");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // Note: Depending on what `out` is, you may need to call `out.close()`.
        // This is usually the case for file output, but not for servlet output.
    }
}
