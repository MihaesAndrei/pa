package lab4.commands.audio.libraryManager;

import lab4.commands.Command;
import lab4.commands.exceptions.UsageArgsException;
import lab4.utility.FavoritesList;
import lab4.utility.PathUtility;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class FavCommand extends Command {

    public FavCommand() {
        this.COMMAND = "fav";
        this.INFO = "add a specific file to the favorites list, and stored after";
        this.USAGE = "fav [file]";
    }

    @Override
    public void execute(List<String> args) throws UsageArgsException {
        if (args.size() == 1) {
            Path filePath = PathUtility.changeDirectoryTo(args.get(0), this.menuCommand.getCurrentDir());
            if (filePath == null) {
                System.out.println("The system cannot find the path specified.");
            } else if (FavoritesList.addFile(filePath)) {
                System.out.println("File was successfully added...");
            } else System.out.println("File is already in your favorites list.");
        } else if (args.size() > 1 || args.size() == 0) throw new UsageArgsException(USAGE);
        if (FavoritesList.myFavoritesList.size() > 0) FavoritesList.showFavList();
    }

    public void execute(File file) {
        FavoritesList.addFile(Paths.get(file.toString()));
        FavoritesList.showFavList();
    }
}
