package lab4.commands.audio.libraryManager;

import lab4.utility.AudioFileUtility;

import java.awt.*;
import java.io.File;

/**
 * Created by Andrei on 3/28/2016.
 */
public class SearchCommand {

    public SearchCommand() {

    }

    public void execute(File file) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(AudioFileUtility.getURI(file));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
