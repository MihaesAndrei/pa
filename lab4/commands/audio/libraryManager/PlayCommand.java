package lab4.commands.audio.libraryManager;

import lab4.commands.Command;
import lab4.commands.exceptions.UsageArgsException;
import lab4.utility.PathUtility;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class PlayCommand extends Command {

    private Desktop desktop;

    public PlayCommand() {
        this.COMMAND = "play";
        this.INFO = "playback of a file using the native operating system application";
        this.USAGE = "play [file path]";
        desktop = Desktop.getDesktop();
    }

    @Override
    public void execute(List<String> args) throws UsageArgsException {

        switch (args.size()) {
            case 0:
                System.out.println("Too few arguments...");
                break;
            case 1:
                Path path = PathUtility.changeDirectoryTo(args.get(0), this.menuCommand.getCurrentDir());

                if(path == null) {
                    System.out.println("The system cannot find the path specified.");
                    break;
                }

                File audioFile = new File(path.toAbsolutePath().toString());
                if(audioFile.exists()) {
                    try {
                        desktop.open(audioFile);
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else System.out.println("There is no such file");
                if(path == null) {
                    System.out.println("The system cannot find the path specified.");
                    break;
                }
            default:
                throw new UsageArgsException(USAGE);
        }
    }

    public void execute(File file) {
        try {
            desktop.open(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
