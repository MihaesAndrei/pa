package lab4.commands.factory;

import lab4.commands.Command;
import lab4.commands.audio.fileManagement.FindAudioFiles;
import lab4.commands.audio.fileManagement.ListCommand;
import lab4.commands.audio.libraryManager.FavCommand;
import lab4.commands.audio.libraryManager.InfoCommand;
import lab4.commands.audio.libraryManager.PlayCommand;
import lab4.commands.audio.libraryManager.ReportCommand;
import lab4.commands.basic.CdCommand;
import lab4.commands.basic.HelpCommand;
import lab4.commands.basic.ManCommand;
import lab4.commands.basic.QuitCommand;

import java.util.ArrayList;
import java.util.List;

public class CommandFactory {

    public static List<Command> getAudioCommands() {
        List<Command> commandList = new ArrayList<>();
        commandList.add(new FindAudioFiles());
        commandList.add(new InfoCommand());
        commandList.add(new ListCommand());
        commandList.add(new FavCommand());
        commandList.add(new PlayCommand());
        commandList.add(new ReportCommand());
        return commandList;
    }

    public static List<Command> getBasicCommands() {
        List<Command> commandList = new ArrayList<>();
        commandList.add(new CdCommand());
        commandList.add(new HelpCommand());
        commandList.add(new ManCommand());
        commandList.add(new QuitCommand());
        return commandList;
    }
}
