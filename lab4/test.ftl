<html>
<head>
  <title>FavList</title>
</head>
<body>
  <h2>Favorites list:</h2>
  	<p>Path: ${song.path}</p>
  	<ul>
  		<li>Artist: ${song.artist}</li>
  		<li>Title: ${song.title}</li>
  		<li>Album: ${song.album}</li>
  		<li>Year: ${song.year}</li>
  	</ul>
</body>
</html>