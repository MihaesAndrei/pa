package lab5.gui.info;

import javax.swing.*;

public class DirectoryInfo extends JTable {

    private static final String[] COLLUMNNAMES = {"File Name", "Title", "Artist", "Album", "Year"};

    public DirectoryInfo() {
        this.setEnabled(false);
    }
    public DirectoryInfo(Object[][] data) {
        super(data, COLLUMNNAMES);
    }
}
