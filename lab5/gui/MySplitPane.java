package lab5.gui;

import lab5.gui.explorer.popupmenu.PopUpMenu;
import lab5.gui.explorer.ExplorerPanel;
import lab5.gui.info.DirectoryInfo;
import lab5.gui.info.MetaDataInfo;

import javax.swing.*;
import java.awt.*;

public class MySplitPane extends JSplitPane {


    public MySplitPane() {
        MetaDataInfo metaDataInfo = new MetaDataInfo();
        DirectoryInfo directoryInfo = new DirectoryInfo();
        ExplorerPanel explorerPanel = new ExplorerPanel(metaDataInfo, directoryInfo, this);

        explorerPanel.setMinimumSize(new Dimension(300, 300));
        explorerPanel.setMaximumSize(new Dimension(300, 300));
        metaDataInfo.setMinimumSize(new Dimension(300, 300));
        directoryInfo.setMinimumSize(new Dimension(300, 300));

        this.setLeftComponent(explorerPanel);
        this.setRightComponent(metaDataInfo);
        this.setDividerLocation(300);
    }
}
