package lab5.gui;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    private static final String TITLE = "Audio Manager";
    private static final Dimension DIMENSION = new Dimension(700, 400);

    public MainFrame() {
        super(TITLE);
        setSize(DIMENSION);

        add(new MySplitPane());

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
}
