package lab5.gui.explorer;


import lab5.gui.MySplitPane;
import lab5.gui.explorer.tree.FileTree;
import lab5.gui.info.DirectoryInfo;
import lab5.gui.info.MetaDataInfo;

import javax.swing.*;
import java.awt.*;
import java.io.File;


public class ExplorerPanel extends JPanel {

    private FileTree fileTree;
    private JScrollPane fileTreeScroll;

    public ExplorerPanel(MetaDataInfo metaDataInfo, DirectoryInfo directoryInfo, MySplitPane splitPane) {
        fileTree = new FileTree(new File[]{new File("D:\\")});

        fileTree.attachMetaDataInfo(metaDataInfo);
        fileTree.attachDirectoryInfo(directoryInfo);
        fileTree.attachSplitPane(splitPane);

        fileTreeScroll = new JScrollPane(getFileTree());
        add(getFileTreeScroll());
        this.setLayout(new GridLayout());
    }

    public FileTree getFileTree() {
        return fileTree;
    }

    public JScrollPane getFileTreeScroll() {
        return fileTreeScroll;
    }
}
