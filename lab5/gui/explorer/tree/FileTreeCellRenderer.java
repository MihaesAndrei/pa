package lab5.gui.explorer.tree;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class FileTreeCellRenderer extends DefaultTreeCellRenderer {

    private static FileSystemView fileSystemView = FileSystemView.getFileSystemView();

    /* cache to speed the rendering*/
    private Map<String, Icon> iconCache = new HashMap<String, Icon>();
    private Map<File, String> rootNameCache = new HashMap<File, String>();

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus) {

        FileTreeNode fileTreeNode = (FileTreeNode) value;
        File file =fileTreeNode.getFile();
        String filename = "";
        if (file != null) {
            if (fileTreeNode.isFileSystemRoot()) {
                filename = this.rootNameCache.get(file);
                if (filename == null) {
                    filename = fileSystemView.getSystemDisplayName(file);
                    this.rootNameCache.put(file, filename);
                }
            } else {
                filename = file.getName();
            }
        }

        JLabel result = (JLabel) super.getTreeCellRendererComponent(tree, filename, sel, expanded, leaf, row, hasFocus);

        if (file != null) {
            Icon icon = this.iconCache.get(filename);
            if (icon == null) {
                icon = fileSystemView.getSystemIcon(file);
                this.iconCache.put(filename, icon);
            }
            result.setIcon(icon);
        }
        return result;
    }
}
