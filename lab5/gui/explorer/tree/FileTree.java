package lab5.gui.explorer.tree;

import lab4.commands.audio.libraryManager.InfoCommand;
import lab4.commands.audio.libraryManager.PlayCommand;
import lab5.gui.explorer.popupmenu.PopUpMenu;
import lab5.gui.MySplitPane;
import lab5.gui.info.DirectoryInfo;
import lab5.gui.info.MetaDataInfo;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class FileTree extends JTree implements TreeSelectionListener{

    private MetaDataInfo metaDataInfo;
    private DirectoryInfo directoryInfo;
    private MySplitPane splitPane;

    public FileTree(File[] roots) {
        super(new FileTreeNode(roots));

        this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        this.addTreeSelectionListener(this);
        this.setRootVisible(false);
        this.setCellRenderer(new FileTreeCellRenderer());
        this.addMouseListener ( new MouseAdapter()
        {
            public void mousePressed ( MouseEvent e )
            {
                if ( SwingUtilities.isRightMouseButton ( e ) )
                {
                    TreePath path = FileTree.this.getPathForLocation(e.getX(), e.getY());
                    Rectangle pathBounds = FileTree.this.getUI ().getPathBounds ( FileTree.this, path );
                    int row = FileTree.this.getClosestRowForLocation(e.getX(), e.getY());
                    FileTree.this.setSelectionRow(row);
                    if ( pathBounds != null && pathBounds.contains ( e.getX (), e.getY () ) )
                    {
                        JPopupMenu menu = new PopUpMenu(FileTree.this);
                        FileTreeNode fileTreeNode = (FileTreeNode) FileTree.this.getLastSelectedPathComponent();
                        if(!fileTreeNode.getFile().isDirectory()) {
                            menu.show ( FileTree.this, pathBounds.x, pathBounds.y + pathBounds.height );
                        }
                    }
                } else if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                    e.consume();
                    FileTreeNode fileTreeNode = (FileTreeNode) FileTree.this.getLastSelectedPathComponent();
                    File file = fileTreeNode.getFile();
                    if(!file.isDirectory()) {
                        PlayCommand playCommand = new PlayCommand();
                        playCommand.execute(file);
                    }
                }
            }
        } );

    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        FileTreeNode fileTreeNode = (FileTreeNode) this.getLastSelectedPathComponent();


        File file = fileTreeNode.getFile();
        if (!file.isDirectory()) {

            /* Call Info Command */
            InfoCommand infoCommand = new InfoCommand();
            String metadata = infoCommand.execute(file);

            metaDataInfo.setText(metadata);
            splitPane.setRightComponent(metaDataInfo);

        } else {

            /* Call Info Command */
            InfoCommand infoCommand = new InfoCommand();
            Object[][] data = infoCommand.getMetadataTable(file);

            directoryInfo = new DirectoryInfo(data);
            JScrollPane jScrollPane = new JScrollPane(directoryInfo);
            splitPane.setRightComponent(jScrollPane);
        }
    }

    public void attachMetaDataInfo(MetaDataInfo metaDataInfo) {
        this.metaDataInfo = metaDataInfo;
    }

    public void attachDirectoryInfo(DirectoryInfo directoryInfo) {
        this.directoryInfo = directoryInfo;
    }

    public void attachSplitPane(MySplitPane mySplitPane) {
        this.splitPane = mySplitPane;
    }
}
