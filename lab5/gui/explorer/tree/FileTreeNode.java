package lab5.gui.explorer.tree;

import lab4.utility.AudioFileUtility;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class FileTreeNode extends DefaultMutableTreeNode implements TreeNode {

    private File file;                  /* Node file */
    private File[] children;            /* Children of node file */
    private TreeNode parent;            /* Parent node */
    private boolean isFileSystemRoot;   /* Indicates if file is a file system root */

    /**
     * Creates a new file tree node
     *
     * @param file
     *            Node file
     * @param isFileSystemRoot
     *            Indicates if file is a file system root(
     * @param parent
     *            Parent node
     */
    public FileTreeNode(File file, boolean isFileSystemRoot, TreeNode parent) {
        this.file = file;
        this.isFileSystemRoot = isFileSystemRoot;
        this.parent = parent;

//        create children nodes only for directories
        if (file.isDirectory()) {
            List<File> files = new ArrayList<>();

//            add directories to children
            File[] dirs = Arrays.stream(file.listFiles()).filter(dir -> dir.isDirectory() && !dir.isHidden()).toArray(File[]::new);
            for (File dir : dirs) files.add(dir);

//             add audio files to children
            List<Path> pathList = AudioFileUtility.findFiles(Paths.get(file.toString()), 1);
            for (Path path : pathList) files.add(new File(path.toFile().toString()));

//            initialize children array
            this.children = new File[files.size()];
            files.toArray(children);
        }
        if (this.children == null) {
            this.children = new File[0];
        }
    }

    /**
     * Creates a new file tree node
     *
     * @param children
     *          Children files
     */
    public FileTreeNode(File[] children) {
        this.file = null;
        this.parent = null;
        this.children = children;
    }


    @Override
    public TreeNode getChildAt(int childIndex) {
        return new FileTreeNode(this.children[childIndex], this.parent == null, this);
    }

    @Override
    public int getChildCount() {
        return this.children.length;
    }

    @Override
    public TreeNode getParent() {
        return this.parent;
    }

    @Override
    public int getIndex(TreeNode node) {
        FileTreeNode ftn = (FileTreeNode) node;
        for (int i = 0; i < this.children.length; i++) {
            if (ftn.getFile().equals(this.children[i]))
                return i;
        }
        return -1;
    }

    @Override
    public boolean getAllowsChildren() {
        return true;
    }

    @Override
    public boolean isLeaf() {
        return (this.getChildCount() == 0);
    }

    @Override
    public Enumeration children() {
        final int elementCount = this.children.length;
        return new Enumeration<File>() {
            int count = 0;
            @Override
            public boolean hasMoreElements() {
                return this.count < elementCount;
            }

            @Override
            public File nextElement() {
                if (hasMoreElements()) {
                    return FileTreeNode.this.children[this.count++];
                } else {
                    throw new NoSuchElementException("Vector Enumeration");
                }
            }
        };
    }

    public File getFile() {
        return file;
    }

    public boolean isFileSystemRoot() {
        return isFileSystemRoot;
    }
}
