package lab5.gui.explorer.popupmenu;

import lab4.commands.audio.libraryManager.FavCommand;
import lab4.commands.audio.libraryManager.PlayCommand;
import lab4.commands.audio.libraryManager.SearchCommand;
import lab4.utility.AudioFileUtility;
import lab4.utility.FavoritesList;
import lab5.gui.explorer.tree.FileTree;
import lab5.gui.explorer.tree.FileTreeNode;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.Paths;

public class PopUpMenuListener implements ActionListener {

    private FileTree fileTree;

    public PopUpMenuListener(FileTree fileTree) {
        this.fileTree = fileTree;
    }

    public void actionPerformed(ActionEvent event) {
        System.out.println("Popup menu item [" +
                event.getActionCommand() + "] was pressed.");
        FileTreeNode fileTreeNode = (FileTreeNode) fileTree.getLastSelectedPathComponent();
        File file = fileTreeNode.getFile();
        String menuItem = event.getActionCommand();
        switch (menuItem) {
            case "Play":
                PlayCommand playCommand = new PlayCommand();
                playCommand.execute(file);
                break;
            case "Add to Favorites":
                FavCommand favCommand = new FavCommand();
                favCommand.execute(file);
                break;
            case "Search Information":
                SearchCommand searchCommand = new SearchCommand();
                searchCommand.execute(file);
                break;
            default:

        }
    }
}
