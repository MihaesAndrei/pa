package lab5.gui.explorer.popupmenu;

import lab5.gui.explorer.tree.FileTree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;

public class PopUpMenu extends JPopupMenu {

    private FileTree fileTree;
    private PopUpMenuListener popUpMenuListener;

    public PopUpMenu(FileTree fileTree) {
        super();

        this.fileTree = fileTree;
        popUpMenuListener = new PopUpMenuListener(fileTree);

        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<JMenuItem> jMenuItemList = new ArrayList<>(Arrays.asList(
                new JMenuItem("Add to Favorites"),
                new JMenuItem("Play"),
                new JMenuItem("Search Information")
        ));
        for (JMenuItem menuItem : jMenuItemList) {
            add(menuItem);
            menuItem.addActionListener(popUpMenuListener);
        }
        setBorder(new BevelBorder(BevelBorder.RAISED));
    }

}