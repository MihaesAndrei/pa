package lab9;

import lab9.mvc.view.MainFrame;

public class Main {

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(MainFrame::new);
    }
}
