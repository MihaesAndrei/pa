package lab9.mvc.controllers;

import lab9.mvc.view.center.buttons.ExecuteButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

public class ExportButtonController implements ActionListener {

    private Vector<String> columnNames;
    private Vector<Vector<Object>> data;

    public ExportButtonController(ExecuteButton executeButton) {
        ExecuteButtonController executeButtonController = executeButton.getExecuteButtonController();
        columnNames = executeButtonController.getColumnNames();
        data = executeButtonController.getData();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new CreateReport(columnNames, data);
    }
}
