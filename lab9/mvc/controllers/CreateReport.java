package lab9.mvc.controllers;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

import java.awt.*;
import java.util.Vector;

import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;


public class CreateReport {

    private Vector<String> columnNames;
    private Vector<Vector<Object>> data;

    public CreateReport(Vector<String> columnNames, Vector<Vector<Object>> data) {
        this.columnNames = columnNames;
        this.data = data;
        build();
    }

    private void build() {
        StyleBuilder boldStyle         = stl.style().bold();
        StyleBuilder boldCenteredStyle = stl.style(boldStyle).setHorizontalAlignment(HorizontalAlignment.CENTER);
        StyleBuilder columnTitleStyle  = stl.style(boldCenteredStyle)
                .setBorder(stl.pen1Point())
                .setBackgroundColor(Color.LIGHT_GRAY);
        TextColumnBuilder<String>[] columns = new TextColumnBuilder[columnNames.size()];
        for(int i = 0; i < columnNames.size(); i++) {
            columns[i] = col.column(columnNames.get(i), columnNames.get(i), type.stringType());
        }
        try {
            report()//create new report design
                    .setColumnTitleStyle(columnTitleStyle)
                    .highlightDetailEvenRows()
                    .columns(columns)
                    .title(cmp.text("DB Table").setStyle(boldCenteredStyle))//shows report title
                    .pageFooter(cmp.pageXofY())//shows number of page at page footer
                    .setDataSource(createDataSource())//set datasource
                    .show();
        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    private JRDataSource createDataSource() {
        String[] columns = new String[columnNames.size()];
        for(int i = 0; i < columnNames.size(); i++) {
            columns[i]  = columnNames.get(i);
        }

        DRDataSource dataSource = new DRDataSource(columns);
        for (int i = 0; i < data.size(); i++) {
            String[] row = new String[columnNames.size()];
            for(int j = 0; j <columnNames.size(); j++) {
                try {
                    row[j] = data.get(i).get(j).toString();
                } catch (NullPointerException e) {
                    row[j] = "";
                }
            }
            dataSource.add(row);
        }
        return dataSource;
    }

}

