package lab9.mvc.controllers;

import lab9.mvc.model.DBConnection;
import lab9.mvc.view.south.ResultFrame;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.Vector;

public class ExecuteButtonController implements ActionListener {

    private DBConnection dbConnection = new DBConnection();
    private JTextArea consoleText;
    private ResultFrame resultFrame;

    private Vector<String> columnNames = new Vector<>();
    private Vector<Vector<Object>> data = new Vector<>();

    public ExecuteButtonController(JTextArea consoleText, ResultFrame resultFrame) {
        this.consoleText = consoleText;
        this.resultFrame = resultFrame;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String sql = consoleText.getText();
        dbConnection.createConnection();
        Connection connection = dbConnection.getConnection();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            DefaultTableModel tableModel = buildTableModel(resultSet);
            resultFrame.getTable().setModel(tableModel);

        } catch (SQLException e1) {
            e1.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }

    }

    private DefaultTableModel buildTableModel(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
        return new DefaultTableModel(data, columnNames);
    }

    public Vector<String> getColumnNames() {
        return columnNames;
    }

    public Vector<Vector<Object>> getData() {
        return data;
    }
}
