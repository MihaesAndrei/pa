package lab9.mvc.view.south;

import javax.swing.*;

public class ResultFrame extends JInternalFrame {

    private JTable table;
    private JScrollPane jScrollPane;

    public ResultFrame() {
        super("Result");
        table = new JTable();
        jScrollPane = new JScrollPane(table);
        this.getContentPane().add(jScrollPane);
        this.setVisible(true);
    }

    public JTable getTable() {
        return table;
    }

    public void setTable(JTable table) {
        this.table = table;
    }

    public JScrollPane getjScrollPane() {
        return jScrollPane;
    }

    public void setjScrollPane(JScrollPane jScrollPane) {
        this.jScrollPane = jScrollPane;
    }
}
