package lab9.mvc.view.splitters;

import lab9.mvc.view.center.Console;
import lab9.mvc.view.center.MetaDataTree;
import lab9.mvc.view.south.ResultFrame;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

public class MainSplitter extends JSplitPane {

    private Console console;

    public MainSplitter(ResultFrame resultFrame) {

        console = new Console(resultFrame);
        this.setLeftComponent(new MetaDataTree(new DefaultMutableTreeNode("Database Objects")));
        this.setRightComponent(console);
    }

    public Console getConsole() {
        return console;
    }

    public void setConsole(Console console) {
        this.console = console;
    }
}
