package lab9.mvc.view.splitters;

import lab9.mvc.view.south.ResultFrame;

import javax.swing.*;

public class TreeConsoleSplitter extends JSplitPane {

    ResultFrame resultFrame = new ResultFrame();
    MainSplitter mainSplitter = new MainSplitter(resultFrame);
    public TreeConsoleSplitter() {
        this.setOrientation(JSplitPane.VERTICAL_SPLIT);
        this.setLeftComponent(mainSplitter);
        this.setRightComponent(resultFrame);
    }
}
