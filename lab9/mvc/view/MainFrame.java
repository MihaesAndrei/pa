package lab9.mvc.view;

import lab9.mvc.view.splitters.TreeConsoleSplitter;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    private static final String TITLE = "DB Application";
    private static final int width = 600;
    private static final int height = 600;
    private TreeConsoleSplitter treeConsoleSplitter = new TreeConsoleSplitter();

    public MainFrame() {
        super(TITLE);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        addComponents();

        this.setSize(new Dimension(width, height));
        this.setVisible(true);
    }

    private void addComponents() {
        this.add(treeConsoleSplitter);
    }
}
