package lab9.mvc.view.center;

import lab9.mvc.view.center.buttons.ExecuteButton;
import lab9.mvc.view.center.buttons.ExportButton;
import lab9.mvc.view.south.ResultFrame;

import javax.swing.*;
import java.awt.*;

public class Console extends JPanel {

    private static final int width = 300;
    private static final int height = 300;

    private JTextArea consoleText = new JTextArea("SELECT * FROM STUDENTI");
    private ExecuteButton executeButton;

    public Console(ResultFrame resultFrame) {
        super();

        executeButton = new ExecuteButton(consoleText, resultFrame);

        this.setLayout(new BorderLayout());
        this.setMinimumSize(new Dimension(width, height));

        this.add(consoleText, BorderLayout.CENTER);
        this.add(executeButton, BorderLayout.NORTH);
        this.add(new ExportButton(executeButton), BorderLayout.SOUTH);
    }

}
