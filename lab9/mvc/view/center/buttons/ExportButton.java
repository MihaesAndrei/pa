package lab9.mvc.view.center.buttons;

import lab9.mvc.controllers.ExportButtonController;

import javax.swing.*;

public class ExportButton extends JButton {

    public ExportButton(ExecuteButton executeButton) {
        super("Export");
        this.addActionListener(new ExportButtonController(executeButton));
    }
}
