package lab9.mvc.view.center.buttons;

import lab9.mvc.controllers.ExecuteButtonController;
import lab9.mvc.view.south.ResultFrame;

import javax.swing.*;

public class ExecuteButton extends JButton {

    private  ExecuteButtonController executeButtonController;

    public ExecuteButton(JTextArea consoleText, ResultFrame resultFrame) {
        super("Execute command");
        executeButtonController = new ExecuteButtonController(consoleText, resultFrame);
        this.addActionListener(executeButtonController);
    }

    public ExecuteButtonController getExecuteButtonController() {
        return executeButtonController;
    }

    public void setExecuteButtonController(ExecuteButtonController executeButtonController) {
        this.executeButtonController = executeButtonController;
    }
}
