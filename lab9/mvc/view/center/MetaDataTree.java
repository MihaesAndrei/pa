package lab9.mvc.view.center;

import lab9.mvc.model.DBConnection;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MetaDataTree extends JTree {

    private DefaultMutableTreeNode tables;
    private DefaultMutableTreeNode procedures;

    public MetaDataTree(DefaultMutableTreeNode top) {
        super(top);

        tables = new DefaultMutableTreeNode("Tables");
        top.add(tables);
        procedures = new DefaultMutableTreeNode("Procedures");
        top.add(procedures);


        DBConnection dbConnection = new DBConnection();
        dbConnection.createConnection();
        Connection connection = dbConnection.getConnection();

        try {
            DatabaseMetaData metaData = connection.getMetaData();
            addTables(metaData);
            addProcedures(metaData);
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    private void addProcedures(DatabaseMetaData metaData) throws SQLException {
        ResultSet resultSet;
        resultSet = metaData.getProcedures(null, "STUDENT", null);
        while (resultSet.next()) {
            String procedure_name = resultSet.getString("PROCEDURE_NAME");
            procedures.add(new DefaultMutableTreeNode(procedure_name));
        }
    }

    private void addTables(DatabaseMetaData metaData) throws SQLException {
        ResultSet resultSet;
        resultSet = metaData.getTables(null, "STUDENT", null, null);
        while (resultSet.next()) {
            String tableName = resultSet.getString("TABLE_NAME");
            tables.add(new DefaultMutableTreeNode(tableName));
        }
    }
}
