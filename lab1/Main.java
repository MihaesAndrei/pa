package lab1;

public class Main {

    private static final String FORMAT = "[n:number of elements] [S:\"elements of the first set\"] [T:\"elements of the second set\"]";
    private static int n;
    private static char[] S;
    private static char[] T;

    public static void main(String[] args) {
        final long startTime = System.nanoTime();

        if (!verifyArgs(args)) System.out.println(FORMAT);
        else {
            Matrix m;
            if (args.length == 0) { // generate random value for n, S, T
                m = new Matrix();
            } else { // parse the command line arguments
                n = Integer.parseInt(args[0]);
                S = args[1].toCharArray();
                T = args[2].toCharArray();
                m = new Matrix(n, S, T);
            }

            System.out.println("Total soltions: " + m.getTotalSolutions());
            System.out.println("First solution:\n" + m);

            generateExample();
        }

        final long duration = System.nanoTime() - startTime;
        System.out.println("Running time: " + (float) duration / 1000000000);
    }

    public static Boolean verifyArgs(String[] args) {
        if (args.length == 0) return true;
        else return !((args.length < 3 && args.length > 0) ||
                Integer.parseInt(args[0]) > args[1].length() ||
                Integer.parseInt(args[0]) > args[2].length());
    }

    public static void generateExample() {
        n = 3;
        S = new char[]{'A', 'B', 'C'};
        T = new char[]{'\u03B1', '\u03B2', '\u03B3'};
        Matrix example = new Matrix(n, S, T);
        int a[][][] = {
                {{0, 1, 2}, {1, 2, 0}, {2, 0, 1}},
                {{0, 2, 1}, {1, 0, 2}, {2, 1, 0}}
        };
        example.setArray(a);
        System.out.println("Example:\n" + example);
    }
}