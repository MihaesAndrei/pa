package lab1;

public class Matrix {
    private int[][][] a;
    private int n;
    private char[] S, T;
    private int totalSolutions;

    public Matrix() {
        n = 1 + (int) (Math.random() * (25 - 1)); // random value between [1, 25)
        S = new char[n];
        T = new char[n];
        for (int i = 0; i < n; i++) {
            S[i] = (char) ('A' + i); // latin Alphabet(only 25 characters)
            if (i != 25) T[i] = (char) ('\u03B1' + i); // greek Alphabet(only 24 characters)
        }
    }

    public Matrix(int n, char[] S, char[] T) {
        this.n = n;
        this.S = S;
        this.T = T;

        Utility.Result r = Utility.getSolution(n);
        setArray(r.a);
        totalSolutions = r.solutions;
    }

    void setArray(int[][][] a) {
        this.a = a;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                result.append(S[a[0][i][j]]);
                result.append(T[a[1][i][j]]);
                result.append(" ");
            }
            result.append("\n");
        }
        return result.toString();
    }

    public int getTotalSolutions() {
        return totalSolutions;
    }
}
