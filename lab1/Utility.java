package lab1;

public class Utility {

    public static class Result {
        public int solutions;
        public int a[][][];

        public Result(int solutions, int a[][][]) {
            this.solutions = solutions;
            this.a = a;
        }
    }

    private static int n;
    private static int solutions = 0;
    private static int[][] v;
    private static int[] list;
    private static int[][][] a;

    public static Result getSolution(int n) {
        Utility.n = n;
        v = new int[n][n];

        initializeList();
        backTracking(0);

        Result r = new Result(solutions, a);
        return r;
    }

    private static void initializeList() {
        list = new int[n * n];
        for (int i = 0; i < n * n; i++) {
            list[i] = (i / n + 1) * 10 + i % n + 1;
        }
    }

    private static void backTracking(int k) {
        int i;
        for (i = 0; i < list.length; i++) {
            v[k / n][k % n] = list[i];
            if (isValidElement(k)) {
                if (isSolution(k)) {
//                    showSolution(k);
                    solutions++;
                    if (solutions == 1) {
                        setArray();
                    }
                } else backTracking(k + 1);
            }
        }
    }

    private static boolean isValidElement(int k) {
        int i;
        for (i = 0; i < k % n; i++) {
            if (v[k / n][i] / 10 == v[k / n][k % n] / 10 || v[k / n][i] % 10 == v[k / n][k % n] % 10)
                return false; // same values on same line
        }
        int j;
        for (j = 0; j < k / n; j++) {
            if (v[j][k % n] / 10 == v[k / n][k % n] / 10 || v[j][k % n] % 10 == v[k / n][k % n] % 10)
                return false; // same values on same collumn
        }
        for (i = 0; i < k / n; i++) { // same value in matrix
            if (i == k / n) { // iteration over last line
                for (j = 0; j < k % n; j++) { // last line will have only k elements
                    if (v[i][j] == v[k / n][k % n]) return false;
                }
            } else {
                for (j = 0; j < n; j++) {
                    if (v[i][j] == v[k / n][k % n]) return false;
                }
            }
        }
        return true;
    }

    private static boolean isSolution(int k) {
        if (k == n * n - 1) return true;
        return false;
    }

    private static void showSolution(int k) {
        System.out.println("Solution " + solutions);
        int i;
        for (i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(v[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void setArray() {
        a = new int[2][n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[0][i][j] = v[i][j] / 10 - 1;
                a[1][i][j] = v[i][j] % 10 - 1;
            }
        }
    }
}