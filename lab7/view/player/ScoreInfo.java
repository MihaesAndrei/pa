package lab7.view.player;


import javafx.scene.text.Text;
import lab7.model.tile.MatrixTile;
import lab7.model.tile.Tile;
import lab7.view.tile.RandomTileButton;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class ScoreInfo extends Text {

    private String info = "Score: 0";
    private String textScore = "Score: ";
    private int score;
    private List<RandomTileButton> selectedTiles;

    public ScoreInfo(List<RandomTileButton> selectedTiles) {
        super();
        this.setText(info);
        this.selectedTiles = selectedTiles;
    }

    public ScoreInfo(Text text) {
        super();
        this.setText(info);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score += score;
        this.info = this.textScore + (this.score + score);
        this.setText(this.info);
    }

    public List<RandomTileButton> getSelectedTiles() {
        return selectedTiles;
    }

    public void setSelectedTiles(List<RandomTileButton> selectedTiles) {
        this.selectedTiles = selectedTiles;
    }

    public int getScore(String word) {
        Properties prop = new Properties();
        int totalScore = 0;
        try (InputStream input = new FileInputStream("resources/letter.properties")) {

            prop.load(input);


            for(int i = 0; i < word.length(); i++) {
                int letterScore = Integer.parseInt(prop.getProperty(word.charAt(i) + ".score"));
                totalScore += letterScore;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return totalScore;
    }
}
