package lab7.view.player;

import javafx.scene.control.Button;
import lab7.controller.SubmitWordController;

import java.util.List;

public class SubmitWordButton extends Button {

    private ScoreInfo scoreInfo;

    public SubmitWordButton(ScoreInfo scoreInfo, PlayerPane playerPane, List<ComputerPane> computerPaneList) {
        super();
        this.setText("Submit");
        this.scoreInfo = scoreInfo;

        this.setOnAction(new SubmitWordController(scoreInfo, playerPane, computerPaneList));

    }
}
