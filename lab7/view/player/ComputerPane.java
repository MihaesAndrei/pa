package lab7.view.player;

import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import lab7.model.player.Computer;
import lab7.model.tile.Tile;
import lab7.view.tile.TileButton;
import lab7.view.tile.TileGridPane;

import java.util.List;

public class ComputerPane extends GridPane implements Runnable{

    private Computer computer;
    private Text wordText = new Text();
    private ScoreInfo scoreInfo = new ScoreInfo(wordText);
    private TileGridPane tileGridPane;

    public ComputerPane(String computerName, TileGridPane tileGridPane) {
        super();
        this.setPadding(new Insets(10));

        this.tileGridPane = tileGridPane;
        this.computer = new Computer(computerName);
        generateTiles();

        this.add(new Text(computer.getName()), 0, 0);
        this.add(new Text(""), 0, 1);
        this.add(new Text("Tiles: "), 0, 2);
        showRandomTiles();
        this.add(new Text(""), 0, 3);
        this.add(new Text("Word: "), 0, 4);
        this.add(wordText, 1, 4);
        this.add(new Text(""), 0, 5);
        this.add(scoreInfo, 0, 6);
    }

    public void generateTiles() {
        if(computer.getTiles() != null) computer.getTiles().clear();
        computer.setTiles(tileGridPane.getMatrixTile().getRandomTiles(7));
        hideTiles(computer.getTiles());
        showRandomTiles();
    }

    public void showRandomTiles() {
        for(int i = 0; i < computer.getTiles().size(); i++) {
            TileButton tileButton = new TileButton(computer.getTiles().get(i));
            this.add(tileButton, 1 + i, 2);
        }
    }

    public synchronized void hideTiles(List<Tile> tiles) {
        for(int i = 0; i < tileGridPane.getTileButtons().size(); i++) {
            for(int j = 0; j < tiles.size(); j++) {
                if (tileGridPane.getTileButtons().get(i).getTile() == tiles.get(j)) {
                    tileGridPane.getTileButtons().get(i).setVisible(false);
                    tileGridPane.getTileButtons().remove(i);
                }
            }
        }
    }


    @Override
    public void run() {
        String word = computer.findWord(computer.getLetters());
        System.out.println(word);
        wordText.setText(word);
        scoreInfo.setScore(scoreInfo.getScore(word));
    }


}
