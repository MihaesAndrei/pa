package lab7.view.player;

import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import lab7.model.player.Player;
import lab7.model.tile.Tile;
import lab7.view.menu.BackButton;
import lab7.view.tile.RandomTileButton;
import lab7.view.tile.TileGridPane;

import java.util.ArrayList;
import java.util.List;

public class PlayerPane extends GridPane {

    private Player player;
    private TileGridPane tileGridPane;
    private List<RandomTileButton> selectedTiles;

    public PlayerPane(String  playerName, TileGridPane tileGridPane, Stage primaryStage, List<ComputerPane> computerPaneList) {
        super();
        this.setPadding(new Insets(10));

        this.tileGridPane = tileGridPane;
        selectedTiles = new ArrayList<>();

        player = new Player(playerName);
        player.setTiles(tileGridPane.getMatrixTile().getRandomTiles(7));

        hideTiles(player.getTiles());

        this.add(new Text(player.getName()), 0, 0);
        this.add(new Text(""), 0, 1);
        this.add(new Text("Tiles: "), 0, 2);
        showRandomTiles();
        this.add(new Text(""), 0, 3);
        this.add(new Text("Word: "), 0, 4);
        this.add(new Text(""), 0, 5);



        ScoreInfo scoreInfo = new ScoreInfo(selectedTiles);

        SubmitWordButton submitWordButton = new SubmitWordButton(scoreInfo, this, null);
        this.add(submitWordButton, 10, 0);
        submitWordButton.setVisible(false);


        this.add(new SubmitWordButton(scoreInfo, this, computerPaneList), 11, 0);
        this.add(scoreInfo, 11, 2);
        this.add(new Text(""), 11, 3);
        this.add(new BackButton(primaryStage), 11, 4);

    }

    public synchronized void hideTiles(List<Tile> tiles) {
        for(int i = 0; i < tileGridPane.getTileButtons().size(); i++) {
            for(int j = 0; j < tiles.size(); j++) {
                if (tileGridPane.getTileButtons().get(i).getTile() == tiles.get(j)) {
                    tileGridPane.getTileButtons().get(i).setVisible(false);
                    tileGridPane.getTileButtons().remove(i);
                }
            }
        }
    }

    public void addSelectedTile(RandomTileButton randomTileButton) {
        selectedTiles.add(randomTileButton);
    }

    public List<RandomTileButton> getSelectedTiles() {
        return selectedTiles;
    }

    public void remove(RandomTileButton randomTileButton) {
        selectedTiles.remove(randomTileButton);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setSelectedTiles(List<RandomTileButton> selectedTiles) {
        this.selectedTiles = selectedTiles;
    }

    public TileGridPane getTileGridPane() {
        return tileGridPane;
    }

    public void setTileGridPane(TileGridPane tileGridPane) {
        this.tileGridPane = tileGridPane;
    }

    public void hideSelectedTiles() {
        for (int i = 0; i < this.getSelectedTiles().size(); i++) {
            this.getSelectedTiles().get(i).setVisible(false);
        }
    }

    public void showRandomTiles() {
        for(int i = 0; i < player.getTiles().size(); i++) {
            RandomTileButton tileButton = new RandomTileButton(player.getTiles().get(i), this);
            this.add(tileButton, 1 + i, 2);
        }
    }

}
