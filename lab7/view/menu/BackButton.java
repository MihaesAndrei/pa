package lab7.view.menu;

import javafx.scene.control.Button;
import javafx.stage.Stage;
import lab7.controller.BackController;

public class BackButton extends Button {

    public BackButton(Stage primaryStage) {
        super();
        this.setText("Back to Main Menu");
        this.setOnAction(new BackController(primaryStage));
    }
}
