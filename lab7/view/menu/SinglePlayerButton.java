package lab7.view.menu;


import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lab7.controller.SinglePlayerController;

public class SinglePlayerButton extends Button{

    public SinglePlayerButton(Stage primaryStage, TextField nameField) {
        super();
        this.setText("Single Player");

        this.setOnAction(new SinglePlayerController(primaryStage, nameField));
    }
}
