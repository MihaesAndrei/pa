package lab7.view.menu;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import lab7.controller.VsComputerController;

public class VsComputerButton extends Button{


    public VsComputerButton(Stage primaryStage, TextField fieldName) {
        super();
        this.setText("Against Computer");

        this.setOnAction(new VsComputerController(primaryStage, fieldName));
    }
}
