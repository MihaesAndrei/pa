package lab7.view.tile;

import javafx.scene.control.Button;
import lab7.model.tile.Tile;

public class TileButton extends Button {

    private Tile tile;

    public TileButton(Tile tile) {
        super();
        this.setMaxSize(30, 30);
        this.setMinSize(30, 30);

        this.tile = tile;
        this.setText(tile.getLetter());
    }

    public Tile getTile() {
        return tile;
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }

}
