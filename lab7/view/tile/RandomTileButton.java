package lab7.view.tile;

import lab7.controller.AddTileController;
import lab7.controller.RemoveTileController;
import lab7.model.tile.Tile;
import lab7.view.player.PlayerPane;

public class RandomTileButton extends TileButton {

    private PlayerPane playerPane;

    public RandomTileButton(Tile tile, PlayerPane playerPane) {
        super(tile);
        this.playerPane = playerPane;
        this.setOnAction(new AddTileController(playerPane));
    }

    public RandomTileButton(RandomTileButton randomTileButton, PlayerPane playerPane) {
        super(randomTileButton.getTile());
        this.setOnAction(new RemoveTileController(playerPane));
    }

    @Override
    public String toString() {
        return this.getTile().getLetter();
    }

}
