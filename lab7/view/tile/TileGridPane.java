package lab7.view.tile;

import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import lab7.model.tile.MatrixTile;

import java.util.ArrayList;
import java.util.List;

public class TileGridPane extends GridPane {

    private MatrixTile matrixTile;
    private List<TileButton> tileButtons;

    public TileGridPane(MatrixTile matrixTile) {
        super();
        this.setPadding(new Insets(10));


        tileButtons = new ArrayList<>();
        this.matrixTile = matrixTile;

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                TileButton btn = new TileButton(matrixTile.getTile(i, j));
                tileButtons.add(btn);
                this.add(btn, j, i);
            }
        }
    }

    public MatrixTile getMatrixTile() {
        return matrixTile;
    }

    public List<TileButton> getTileButtons() {
        return tileButtons;
    }

    public int availableTiles() {
        int counter = 0;
        for(int i = 0; i < tileButtons.size(); i++) {
            if(tileButtons.get(i).isVisible()) counter++;
        }
        return counter;
    }
}
