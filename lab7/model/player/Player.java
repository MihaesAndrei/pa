package lab7.model.player;

import lab7.model.tile.Tile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Player implements Serializable {

    private String name;
    private List<Tile> randomTiles;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tile> getTiles() {
        return randomTiles;
    }

    public void setTiles(List<Tile> tiles) {
        this.randomTiles = tiles;
    }

    @Override
    public String toString() {
        return name + ": " + getTiles();
    }
}
