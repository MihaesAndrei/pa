package lab7.model.player;

import lab7.model.tile.Tile;
import lab7.model.vocabulary.Vocabulary;
import lab7.model.vocabulary.VocabularyFactory;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Computer extends Player{

    private Vocabulary vocabulary;
    private boolean found = false;
    private String word;

    public Computer(String name) {
        super(name);
        vocabulary = VocabularyFactory.createContext("resources/words.txt");
    }

    public String getLetters() {
        List<Tile> tileList = this.getTiles();
        System.out.println(tileList);
        String word = "";
        for (Tile tile : tileList) {
            word += tile.getLetter();
        }
        return word;
    }

    public String findWord(String letters) {
        found = false;
        for(int i = letters.length()-1; i >= 1 ; i--) {
            generate(letters, i);
        }
        return word;
    }


    public void generate(String input, int N) {
        generate("", input, new HashSet<>(), N);
    }

    private void generate(String str, String input, Set<String> dup, int N) {
        if(!found) {
            if (str.length() == N && dup.add(str)) {
//            System.out.println(str);
                if (vocabulary.contains(str.toLowerCase())) {
//                    System.out.println(str);
                    word = str;
                    found = true;
                }
            } else
                //remove a char form input and add it to str
                for (int i = 0; i < input.length(); i++)
                    generate(
                            str + input.charAt(i),
                            input.substring(0, i) + input.substring(i + 1),
                            dup, N);
        }
    }


}
