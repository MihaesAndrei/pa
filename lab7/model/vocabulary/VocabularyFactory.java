package lab7.model.vocabulary;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class VocabularyFactory {

    public static Vocabulary createContext(String filename) {
        Vocabulary vocabulary = new TreeVocabulary();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filename))) {
            String word;
            while ((word = bufferedReader.readLine()) != null) {
                vocabulary.add(word);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return vocabulary;
    }
}
