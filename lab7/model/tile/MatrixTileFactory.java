package lab7.model.tile;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class MatrixTileFactory {

    public static MatrixTile createContext(String fileName) {
        Properties prop = new Properties();
        MatrixTile matrixTile = null;

        try (InputStream input = new FileInputStream("resources/letter.properties")) {

            prop.load(input);

            List<Tile> list = new ArrayList<>();

            for (int i = 0; i <= 25; i++) {
                String letter = String.valueOf((char) (i + 65));

                int distribution = Integer.parseInt(prop.getProperty(letter + ".distribution"));
                for(int j = 0; j < distribution; j++) {
                    list.add(new Tile(letter, Integer.parseInt(prop.getProperty(letter + ".score"))));
                }
            }

            Collections.sort(list);
            list.add(new Tile(" ", 0));
            list.add(new Tile(" ", 0));

            matrixTile = new MatrixTile(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matrixTile;
    }
}
