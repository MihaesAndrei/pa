package lab7.model.tile;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MatrixTile {

    private List<Tile> tileList;

    public MatrixTile() {
        tileList = new ArrayList<>();
    }

    public MatrixTile(List<Tile> tileList) {
        this.tileList = tileList;
    }

    public void addTile(Tile tile) {
        tileList.add(tile);
    }

    public void removeTile(Tile tile) {
        tileList.remove(tile);
    }

    public Tile getTile(int i, int j) {
        return tileList.get(i * 10 + j);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                result.append(tileList.get(10 * i + j) + " ");
            }
            result.append("\n");
        }
        return result.toString();
    }

    public List<Tile> getRandomTiles(int count) {
        List<Tile> tiles = new ArrayList<>();
        Random randomGenerator = new Random();

        ArrayList<Integer> numbers = new ArrayList<>();

        while (numbers.size() < count) {
            int random = randomGenerator.nextInt(100);
            if (!numbers.contains(random)) {
                numbers.add(random);
            }
        }

        for(int i = 0; i < numbers.size(); i++) {
            tiles.add(tileList.get(numbers.get(i)));
        }
        return tiles;
    }
}
