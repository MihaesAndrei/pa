package lab7.model.tile;

public class Tile implements Comparable<Tile>{

    private String letter;
    private int score;

    public Tile(String letter, int score) {
        this.letter = letter;
        this.score = score;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return letter;
    }

    @Override
    public int compareTo(Tile o) {
        return this.getLetter().compareTo(o.getLetter());
    }
}
