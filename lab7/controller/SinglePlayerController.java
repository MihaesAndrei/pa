package lab7.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import lab7.model.tile.MatrixTileFactory;
import lab7.view.player.PlayerPane;
import lab7.view.tile.TileGridPane;

public class SinglePlayerController implements EventHandler<ActionEvent> {

    private Stage primaryStage;
    private TextField nameField;

    public SinglePlayerController(Stage primaryStage, TextField nameField) {
        this.primaryStage = primaryStage;
        this.nameField = nameField;
    }

    @Override
    public void handle(ActionEvent event) {

        GridPane singlePlayerGrid = new GridPane();

        TileGridPane tileGridPane = new TileGridPane(MatrixTileFactory.createContext("resources/letter.properties"));
        singlePlayerGrid.add(tileGridPane, 0, 0);

        singlePlayerGrid.add(new PlayerPane(nameField.getText(), tileGridPane, primaryStage, null), 2, 0);

        primaryStage.setScene(new Scene(singlePlayerGrid, 800, 400));
    }
}
