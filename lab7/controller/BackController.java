package lab7.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import lab7.view.menu.SinglePlayerButton;
import lab7.view.menu.VsComputerButton;

public class BackController implements EventHandler<ActionEvent> {

    private Stage primaryStage;

    public BackController(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }


    @Override
    public void handle(ActionEvent event) {
        TextField nameField = new TextField();

        VBox mainGrid = new VBox();
        mainGrid.setPadding(new Insets(10, 50, 50, 50));
        mainGrid.setSpacing(10);
        mainGrid.getChildren().add(new SinglePlayerButton(primaryStage, nameField));
        mainGrid.getChildren().add(new VsComputerButton(primaryStage, nameField));
        FlowPane flowPane = new FlowPane();
        flowPane.getChildren().add(new Label("Name: "));
        flowPane.getChildren().add(nameField);
        flowPane.setAlignment(Pos.CENTER);

        mainGrid.getChildren().add(flowPane);
        mainGrid.setAlignment(Pos.CENTER);
        primaryStage.setScene(new Scene(mainGrid, 800, 400));
    }
}
