package lab7.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import lab7.model.player.Computer;
import lab7.model.tile.MatrixTileFactory;
import lab7.view.player.ComputerPane;
import lab7.view.player.PlayerPane;
import lab7.view.tile.TileGridPane;

import java.util.ArrayList;
import java.util.List;

public class VsComputerController implements EventHandler<ActionEvent> {

    private Stage primaryStage;
    private TextField nameField;

    public VsComputerController(Stage primaryStage, TextField nameField) {
        this.primaryStage = primaryStage;
        this.nameField = nameField;
    }

    @Override
    public void handle(ActionEvent event) {
        GridPane vsComputerGrid = new GridPane();
        TileGridPane tileGridPane ;
        tileGridPane = new TileGridPane(MatrixTileFactory.createContext("resources/letter.properties"));
        vsComputerGrid.add(tileGridPane, 0, 0);

        ComputerPane computer1 = new ComputerPane("Computer 1", tileGridPane);
        vsComputerGrid.add(computer1, 0, 1);
        new Thread(computer1).start();

        ComputerPane computer2 = new ComputerPane("Computer 2", tileGridPane);
        vsComputerGrid.add(computer2, 0, 2);
        new Thread(computer2).start();

        List<ComputerPane> computerPaneList = new ArrayList<>();
        computerPaneList.add(computer1);
        computerPaneList.add(computer2);
        vsComputerGrid.add(new PlayerPane(nameField.getText(), tileGridPane, primaryStage, computerPaneList), 2, 0);



        primaryStage.setScene(new Scene(vsComputerGrid, 800, 650));
    }
}
