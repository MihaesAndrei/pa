package lab7.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import lab7.view.player.PlayerPane;
import lab7.view.tile.RandomTileButton;

public class AddTileController implements EventHandler<ActionEvent>{

    private PlayerPane playerPane;

    public AddTileController(PlayerPane playerPane) {
        this.playerPane = playerPane;
    }

    @Override
    public void handle(ActionEvent event) {
        RandomTileButton randomTileButton = (RandomTileButton)event.getSource();
        randomTileButton.setVisible(true);

        RandomTileButton newTile = new RandomTileButton(randomTileButton, playerPane);

        playerPane.addSelectedTile(newTile);
        playerPane.add(newTile, playerPane.getSelectedTiles().size(), 4);
    }
}
