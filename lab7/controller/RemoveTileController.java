package lab7.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import lab7.view.player.PlayerPane;
import lab7.view.tile.RandomTileButton;

public class RemoveTileController implements EventHandler<ActionEvent> {


    private PlayerPane playerInfo;

    public RemoveTileController(PlayerPane playerInfo) {
        this.playerInfo = playerInfo;
    }

    @Override
    public void handle(ActionEvent event) {
        RandomTileButton randomTileButton1 = (RandomTileButton) event.getSource();
        randomTileButton1.setVisible(false);
        playerInfo.remove(randomTileButton1);
    }
}
