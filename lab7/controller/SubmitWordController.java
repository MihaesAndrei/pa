package lab7.controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import lab7.model.vocabulary.Vocabulary;
import lab7.model.vocabulary.VocabularyFactory;
import lab7.view.player.ComputerPane;
import lab7.view.player.PlayerPane;
import lab7.view.player.ScoreInfo;
import lab7.view.tile.RandomTileButton;

import java.util.List;

public class SubmitWordController implements EventHandler<ActionEvent> {

    private Vocabulary vocabulary;
    private ScoreInfo scoreInfo;
    private PlayerPane playerPane;
    private List<ComputerPane> computerPanes;
    public SubmitWordController(ScoreInfo scoreInfo, PlayerPane playerPane, List<ComputerPane> computerPanes) {
        this.scoreInfo = scoreInfo;
        this.playerPane = playerPane;
        this.computerPanes = computerPanes;

        vocabulary = VocabularyFactory.createContext("resources/words.txt");
    }

    @Override
    public void handle(ActionEvent event) {

        if(playerPane.getTileGridPane().availableTiles() < 7) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Word Information");
            alert.setHeaderText(null);
            alert.setContentText("Game Over!");

            alert.showAndWait();
        } else {
            List<RandomTileButton> selectedTiles = scoreInfo.getSelectedTiles();

            String wordSelected = "";
            int totalScore = 0;

            for (int i = 0; i < selectedTiles.size(); i++) {
                wordSelected += selectedTiles.get(i).getTile().getLetter();
                totalScore += selectedTiles.get(i).getTile().getScore();
            }

            if (vocabulary.contains(wordSelected.toLowerCase())) {

                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Word Information");
                alert.setHeaderText(null);
                alert.setContentText("Word '" + wordSelected + "' is correct.\nScore: " + totalScore);

                alert.showAndWait();

                scoreInfo.setScore(totalScore);

                playerPane.getPlayer().setTiles(playerPane.getTileGridPane().getMatrixTile().getRandomTiles(7));
                playerPane.hideTiles(playerPane.getPlayer().getTiles());

                for (int i = 0; i < playerPane.getPlayer().getTiles().size(); i++) {
                    RandomTileButton tileButton = new RandomTileButton(playerPane.getPlayer().getTiles().get(i), playerPane);
                    playerPane.add(tileButton, 1 + i, 2);
                }

                playerPane.hideSelectedTiles();
                playerPane.getSelectedTiles().clear();

                for(int i = 0; i < computerPanes.size(); i++) {
                    try {
                        computerPanes.get(i).generateTiles();
                        new Thread(computerPanes.get(i)).start();
                    } catch(IndexOutOfBoundsException e) {
                        Alert gameOver = new Alert(Alert.AlertType.WARNING);
                        gameOver.setTitle("Game Information");
                        gameOver.setHeaderText(null);
                        gameOver.setContentText("Game Over! Not Enough tiles remaining.");

                        gameOver.showAndWait();
                    }
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Word Information");
                alert.setHeaderText(null);
                alert.setContentText("Invalid Word");

                alert.showAndWait();

                playerPane.hideSelectedTiles();
                playerPane.getSelectedTiles().clear();
            }
        }
    }
}
