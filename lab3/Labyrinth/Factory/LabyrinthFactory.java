package lab3.labyrinth.factory;

import lab3.labyrinth.model.Labyrinth;
import lab3.labyrinth.model.MatrixLabyrinth;

import java.io.*;

public class LabyrinthFactory {

    public static Labyrinth getLabyrinthFromMatrix(int[][] matrix) {
        return new MatrixLabyrinth(matrix);
    }

    public static Labyrinth getLabyrinthFromFile(String filename) {

        int[][] labyrinthMatrix = new int[5][5];
        int columnNumber;
        int rowNumber;
        FileReader file = null;
        try {
            file = new FileReader(new File(filename));

            BufferedReader br = new BufferedReader(file);
            String temp;
            int i = 0;
            String[] s;
            while ((temp = br.readLine()) != null) {
                s = temp.split(" ");

                for (int j = 0; j < s.length; j++) {
                    columnNumber = s.length;
                    labyrinthMatrix[i][j]=Integer.parseInt(s[j]);
                }
                i++;
            }
            rowNumber = i ;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new MatrixLabyrinth(labyrinthMatrix);
    }

    public static Labyrinth getRandomLabyrinth() {
        int[][] labyrinthMatrix ;
        int columnNumber;
        int rowNumber;

        rowNumber = (int) ((Math.random()+0.034) * 30);
        columnNumber = (int) ((Math.random()+0.034) * 30);
        labyrinthMatrix = new int[rowNumber][columnNumber];
        int startCellRow = (int) ((Math.random()+0.034) * 30)%rowNumber;
        int startCellColumn = (int) ((Math.random()+0.034) * 30)%columnNumber;
        int finishCellRow = (int) ((Math.random()+0.034) * 30)%rowNumber;
        int finishCellColumn = (int) ((Math.random()+0.034) * 30)%columnNumber;
        for (int i = 0; i < rowNumber; i++) {
            for (int j = 0; j < columnNumber; j++) {
                if(i==startCellRow && j==startCellColumn){
                    labyrinthMatrix[i][j] = -1;
                    continue;
                }
                if(i==finishCellRow && j==finishCellColumn){
                    labyrinthMatrix[i][j] = 2;
                    continue;
                }
                labyrinthMatrix[i][j] = (int)Math.round(Math.random());
            }
        }
        return new MatrixLabyrinth(labyrinthMatrix);
    }
}
