package lab3.labyrinth.model;

public class MatrixLabyrinth implements Labyrinth {

    private int[][] matrix;
    private int rows;
    private int columns;

    public MatrixLabyrinth(int[][] matrix) {
        this.rows = matrix.length;
        this.columns = matrix[0].length;
        this.matrix = matrix;
    }

    public int getRowCount() {
        return rows;
    }

    public int getColumnCount() {
        return columns;
    }

    public boolean isFreeAt(int row, int column) {
        return matrix[row][column] == 0 ||
                matrix[row][column] == -1 ||
                matrix[row][column] == 2;
    }

    public boolean isWallAt(int row, int column) {
        return matrix[row][column] == 1;
    }

    public int[] getStartCell() {
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                if (matrix[row][column] == -1) return new int[]{row, column};
            }
        }
        return new int[]{-1, -1};
    }

    public int[] getFinishCell() {
        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                if (matrix[row][column] == 2) return new int[]{row, column};
            }
        }
        return new int[]{-1, -1};
    }

    public int getValue(int row, int column) {
        return matrix[row][column];
    }

    public boolean setValue(int row, int column, int value) {
        if(isOutOfMatrix(row, column)) return false;
        else {
            matrix[row][column] = value;
            return true;
        }
    }

    public boolean isOutOfMatrix(int row, int column) {
        return row >= rows || row < 0 || column >= columns || column < 0;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}