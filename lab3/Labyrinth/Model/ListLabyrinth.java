package lab3.labyrinth.model;

import java.util.ArrayList;
import java.util.LinkedList;

public class ListLabyrinth implements Labyrinth {

    ArrayList<LinkedList<Integer>> list = new ArrayList<LinkedList<Integer>>();
    private int rows;
    private int columns;
    private int[] startCell;
    private int[] finishCell;

    public ListLabyrinth(int[][] matrix) {
        rows = matrix.length;
        columns = matrix[0].length;
        for(int i = 0; i < matrix.length; i++) {
            list.add(new LinkedList<Integer>());
            for(int j = 0; j < matrix[i].length; j++) {
                if(matrix[i][j] == 1) list.get(i).add(j);
                else if(matrix[i][j] == -1) startCell = new int[]{i, j};
                else if(matrix[i][j] == 2) finishCell = new int[]{i, j};
            }
        }
    }


    public int getRowCount() {
        return rows;
    }

    public int getColumnCount() {
        return columns;
    }

    public boolean isFreeAt(int row, int column) {
        return !isWallAt(row, column);
    }

    public boolean isWallAt(int row, int column) {
        return list.get(row).contains(column);
    }

    public int[] getStartCell() {
        return startCell;
    }

    public int[] getFinishCell() {
        return finishCell;
    }

    public int getValue(int row, int column) {
        if(row == startCell[0] && column == startCell[1]) return -1;
        else if(row == finishCell[0] && column == finishCell[1]) return 2;
        else if(isWallAt(row, column)) return 1;
        else return 0;
    }

    public boolean setValue(int row, int column, int value) {
        list.get(row).add(column);
        return true;
    }

    public boolean isOutOfMatrix(int row, int column) {
        return row >= rows || row < 0 || column >= columns || column < 0;
    }
}
