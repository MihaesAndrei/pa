package lab3.labyrinth.model;

public interface Labyrinth {

    int getRowCount();

    int getColumnCount();

    boolean isFreeAt(int row, int column);

    boolean isWallAt(int row, int column);

    int[] getStartCell();

    int[] getFinishCell();

    int getValue(int row, int column);

    boolean setValue(int row, int column, int value);

    boolean isOutOfMatrix(int row, int column);
}
