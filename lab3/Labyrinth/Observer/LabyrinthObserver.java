package lab3.labyrinth.observer;

public interface LabyrinthObserver {

    void processCell(int row, int column);

    void processSolution();

}
