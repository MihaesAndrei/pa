package lab3.labyrinth.observer;

import lab3.labyrinth.model.*;
import lab3.labyrinth.solver.*;

import java.io.*;
import java.util.*;

public class AutomatedObserver implements LabyrinthObserver {

    private LabyrinthSolver labyrinthSolver;

    private List<LabyrinthSolution> solutions = new ArrayList<>();

    private class LabyrinthSolution {
        public String matrix;
        public List<Character> path = new ArrayList<>();

        public LabyrinthSolution(String matrix, List<Character> path) {
            this.matrix = matrix;
            this.path = path;
        }

        public String toString() {
            return "The solution for the above labyrinth: " + path + "\n" + matrix + "\n\n";
        }
    }

    public AutomatedObserver(LabyrinthSolver labyrinthSolver) {
        this.labyrinthSolver = labyrinthSolver;
        this.labyrinthSolver.attachObserver(this);
        System.out.println("AutomatedObserver is running...");
    }

    @Override
    public void processSolution() {
        Labyrinth labyrinth = labyrinthSolver.getLabyrinth();

        int currentRow = labyrinthSolver.getLabyrinth().getStartCell()[0];
        int currentColumn = labyrinthSolver.getLabyrinth().getStartCell()[1];

        List<Character> path = new ArrayList<>();

        StringBuilder matrix = new StringBuilder();
        for (int i = 0; i < labyrinth.getRowCount(); i++) {
            for (int j = 0; j < labyrinth.getColumnCount(); j++) {
                matrix.append("|");
                if (labyrinth.getValue(i, j) == -1) {
                    matrix.append("S");
                } else if (labyrinth.getValue(i, j) > 2) {
                    addMove(currentRow, currentColumn, path, i, j);
                    currentRow = i; currentColumn = j;
                    matrix.append("X");
                    labyrinth.setValue(i, j, 1);
                } else if (labyrinth.getValue(i, j) == 1 || labyrinth.getValue(i, j) == 0) {
                    matrix.append("*");
                } else if (labyrinth.getValue(i, j) == 2) {
                    addMove(currentRow, currentColumn, path, i, j);
                    matrix.append("F");
                }
            }
            matrix.append("|\n");
        }
        solutions.add(new LabyrinthSolution(matrix.toString(), path));

        Collections.sort(solutions, (s1, s2) -> s2.path.size() - s1.path.size());

        storeSolutions();
    }

    private void addMove(int currentRow, int currentColumn, List<Character> path, int i, int j) {
        if(i == currentRow && j == currentColumn + 1) path.add('R');
        else if(i == currentRow && j == currentColumn - 1) path.add('L');
        else if(i == currentRow + 1 && j == currentColumn) path.add('D');
        else if(i == currentRow - 1 && j == currentColumn) path.add('U');
    }

    @Override
    public void processCell(int row, int column) {

    }

    private void storeSolutions() {
        try {
            Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("labyrinthSolutions.txt"), "UTF-8"));
            for (LabyrinthSolution solution : solutions) {
                writer.write("Solution " + (solutions.indexOf(solution) + 1) + '\n');
                writer.write(solution.toString());
            }
            System.out.println("I've stored " + ordinal(solutions.size())+  " solution...");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String ordinal(int i) {
        String[] sufixes = new String[] { "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };
        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + sufixes[i % 10];

        }
    }

}