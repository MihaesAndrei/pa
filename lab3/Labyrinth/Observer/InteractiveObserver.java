package lab3.labyrinth.observer;

import lab3.labyrinth.model.*;
import lab3.labyrinth.solver.*;

public class InteractiveObserver implements LabyrinthObserver {

    private LabyrinthSolver labyrinthSolver;

    public InteractiveObserver(LabyrinthSolver labyrinthSolver) {
        this.labyrinthSolver = labyrinthSolver;
        this.labyrinthSolver.attachObserver(this);
    }

    @Override
    public void processCell(int row, int column) {
        Labyrinth labyrinth = labyrinthSolver.getLabyrinth();

        for(int i = 0; i < labyrinth.getRowCount(); i++) {
            for(int j = 0; j < labyrinth.getColumnCount(); j++) {
                System.out.print("|");
                if(labyrinth.getValue(i, j) > 2) System.out.print("X");
                else if (labyrinth.getValue(i, j) == -1) System.out.print("S");
                else if (labyrinth.getValue(i, j) == 0) System.out.print("*");
                else if (labyrinth.getValue(i, j) == 1) System.out.print("*");
                else if (labyrinth.getValue(i, j) == 2) System.out.print("F");
            }
            System.out.println("|");
        }
        System.out.println();
    }

    @Override
    public void processSolution() {
        Labyrinth labyrinth = labyrinthSolver.getLabyrinth();

        for(int i = 0; i < labyrinth.getRowCount(); i++) {
            for(int j = 0; j < labyrinth.getColumnCount(); j++) {
                System.out.print("|");
                if(labyrinth.getValue(i, j) > 2) System.out.print("X");
                else if (labyrinth.getValue(i, j) == -1) System.out.print("S");
                else if (labyrinth.getValue(i, j) == 2) System.out.print("F");
                else System.out.print("*");
            }
            System.out.println("|");
        }
        System.out.println();
    }

}

