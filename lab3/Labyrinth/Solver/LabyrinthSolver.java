package lab3.labyrinth.solver;

import lab3.labyrinth.model.*;
import lab3.labyrinth.observer.*;

public interface LabyrinthSolver {

    Labyrinth getLabyrinth();

    void setLabyrinth(Labyrinth labyrinth);

    int getPathMark();

    boolean nextCellToExplore(int row, int column);

    void solve();

    void attachObserver(LabyrinthObserver observer);

    void notifyAboutProcessCell(int row, int column);

    void notifyAboutProcessSolution();
}
