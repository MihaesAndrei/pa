package lab3.labyrinth.solver;

import lab3.labyrinth.model.*;
import lab3.labyrinth.observer.*;

import java.util.*;

public class AutomatedSolver implements LabyrinthSolver {


    private Labyrinth labyrinth;
    private List<LabyrinthObserver> observers;

    private int startRow, startColumn;
    private int finishRow, finishColumn;
    private int pathMark = 3;

    public AutomatedSolver(Labyrinth labyrinth) {
        setLabyrinth(labyrinth);
        observers = new ArrayList<>();

        startRow = labyrinth.getStartCell()[0];
        startColumn =  labyrinth.getStartCell()[1];
        finishRow = labyrinth.getFinishCell()[0];
        finishColumn = labyrinth.getFinishCell()[1];
    }

    @Override
    public int getPathMark() {
        return pathMark;
    }

    @Override
    public Labyrinth getLabyrinth() {
        return labyrinth;
    }

    @Override
    public void setLabyrinth(Labyrinth labyrinth) {
        this.labyrinth = labyrinth;
    }

    @Override
    public boolean nextCellToExplore(int row, int column) {
        if(labyrinth.isOutOfMatrix(row, column)) {
            return false;
        } else if(row == finishRow && column == finishColumn) {
            notifyAboutProcessSolution();
            pathMark++;
            return true;
        } else if(!labyrinth.isFreeAt(row, column)) {
            return false;
        } else if(labyrinth.getValue(row, column) == 0) {
            labyrinth.setValue(row, column, pathMark);
        }

        nextCellToExplore(row + 1, column);
        nextCellToExplore(row, column + 1);
        nextCellToExplore(row - 1, column);
        nextCellToExplore(row, column - 1);
        return true;
    }

    @Override
    public void solve() {
        nextCellToExplore(startRow, startColumn);
    }

    @Override
    public void attachObserver(LabyrinthObserver observer) {
        observers.add(observer);
    }

    @Override
    public void notifyAboutProcessCell(int row, int column) {
        for (LabyrinthObserver observer : observers) {
            observer.processCell(row, column);
        }
    }

    @Override
    public void notifyAboutProcessSolution() {
        for (LabyrinthObserver observer : observers) {
            observer.processSolution();
        }
    }
}
