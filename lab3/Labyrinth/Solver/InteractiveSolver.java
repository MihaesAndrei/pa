package lab3.labyrinth.solver;

import lab3.labyrinth.model.*;
import lab3.labyrinth.observer.*;

import java.util.*;

public class InteractiveSolver implements LabyrinthSolver {

    private Labyrinth labyrinth;
    private List<LabyrinthObserver> observers;

    private int startRow, startColumn;
    private int finishRow, finishColumn;
    private int currentRow, currentColumn;
    private int pathMark = 3;


    public InteractiveSolver(Labyrinth labyrinth) {
        setLabyrinth(labyrinth);
        observers = new ArrayList<>();

        startRow = labyrinth.getStartCell()[0];
        startColumn =  labyrinth.getStartCell()[1];
        finishRow = labyrinth.getFinishCell()[0];
        finishColumn = labyrinth.getFinishCell()[1];
        currentRow = startRow;
        currentColumn = startColumn;
    }

    @Override
    public int getPathMark() {
        return pathMark;
    }

    @Override
    public Labyrinth getLabyrinth() {
        return this.labyrinth;
    }

    @Override
    public void setLabyrinth(Labyrinth labyrinth) {
        this.labyrinth = labyrinth;
    }

    @Override
    public boolean nextCellToExplore(int row, int column) {
        if(labyrinth.isOutOfMatrix(row, column)) {
            System.out.println("Wrong Cell: Out of Matrix !");
            return false;
        } else if(labyrinth.isWallAt(row, column)) {
            System.out.println("Wrong Cell: You hit wall!");
            return false;
        } else {
            currentRow = row;
            currentColumn = column;
            if(row == finishRow && column == finishColumn) {
                notifyAboutProcessSolution();
                System.out.println("You Won!");
            }
            else {
                labyrinth.setValue(currentRow, currentColumn, pathMark);
                notifyAboutProcessCell(currentRow, currentColumn);
            }
            return true;
        }
    }

    @Override
    public void solve() {
        Scanner scanIn = new Scanner(System.in);
        String command;

        notifyAboutProcessCell(0, 0);

        while(true) {
            System.out.print("Move: ");
            command = scanIn.nextLine();
            switch (command) {
                case "D":
                    nextCellToExplore(currentRow + 1, currentColumn);
                    break;
                case "U":
                    nextCellToExplore(currentRow - 1, currentColumn );
                    break;
                case "L":
                    nextCellToExplore(currentRow, currentColumn - 1);
                    break;
                case "R":
                    nextCellToExplore(currentRow, currentColumn + 1);
                    break;
                default:
                    System.out.println("Wrong command! Try: D(own), U(p), L(eft), R(ight).");
                    break;
            }
            if(currentRow == finishRow && currentColumn == finishColumn) break;
        }
    }

    @Override
    public void attachObserver(LabyrinthObserver observer) {
        observers.add(observer);
    }

    @Override
    public void notifyAboutProcessCell(int row, int column) {
        for (LabyrinthObserver observer : observers) {
            observer.processCell(row, column);
        }
    }

    @Override
    public void notifyAboutProcessSolution() {
        for (LabyrinthObserver observer : observers) {
            observer.processSolution();
        }
    }
}
