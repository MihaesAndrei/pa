package lab3.labyrinth.view;

import lab3.labyrinth.model.Labyrinth;

public interface LabyrinthView {

    Labyrinth getLabyrinth();

    void setLabyrinth(Labyrinth labyrinth);

    String toString();
}
