package lab3.labyrinth.view;

import lab3.labyrinth.model.Labyrinth;

public class LabyrinthTextView implements LabyrinthView {

    private Labyrinth labyrinth;

    public Labyrinth getLabyrinth() {
        return this.labyrinth;
    }

    public void setLabyrinth(Labyrinth labyrinth) {
        this.labyrinth = labyrinth;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();

        for (int row = 0; row < labyrinth.getRowCount(); row++) {
            for (int column = 0; column < labyrinth.getColumnCount(); column++) {
                result.append("|");
                if (labyrinth.getValue(row, column) == -1) result.append("S");
                else if (labyrinth.getValue(row, column) == 0) result.append(" ");
                else if (labyrinth.getValue(row, column) == 1) result.append("*");
                else if (labyrinth.getValue(row, column) == 2) result.append("F");
            }
            result.append("|\n");
        }
        return result.toString();
    }
}
