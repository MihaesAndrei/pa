package lab3.labyrinth.view;

import lab3.labyrinth.model.*;

public class LabyrinthPathView implements LabyrinthView {

    private Labyrinth labyrinth;
    private int pathMark;
    private int length = 0;

    public LabyrinthPathView(Labyrinth labyrinth, int pathMark) {
        setLabyrinth(labyrinth);
        this.pathMark = pathMark;

    }

    public LabyrinthPathView(LabyrinthPathView labyrinthPathView) {
        this.labyrinth = labyrinthPathView.getLabyrinth();
        this.pathMark = labyrinthPathView.getPathMark();
        this.length = labyrinthPathView.getLength();
    }

    public Labyrinth getLabyrinth() {
        return this.labyrinth;
    }

    public void setLabyrinth(Labyrinth labyrinth) {
        this.labyrinth = labyrinth;
    }

    public int getLength() {
        return length;
    }

    public int getPathMark() {
        return pathMark;
    }

    public String toString() {
        length = 0;
        StringBuilder result = new StringBuilder();

        for (int row = 0; row < labyrinth.getRowCount(); row++) {
            for (int column = 0; column < labyrinth.getColumnCount(); column++) {
                result.append("|");

//                result.append(labyrinth.getValue(row, column));

                if (labyrinth.getValue(row, column) == -1) { result.append("S"); length++; }
                else if (labyrinth.getValue(row, column) == pathMark) { result.append("X"); length++; }
                else if (labyrinth.getValue(row, column) == 2) result.append("F");
                else result.append("*");
            }
            result.append("|\n");
        }
        result.append("Path Length: " + length + "\n");
        return result.toString();
    }
}
