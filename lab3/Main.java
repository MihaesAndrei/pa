package lab3;

import lab3.labyrinth.factory.LabyrinthFactory;
import lab3.labyrinth.model.*;
import lab3.labyrinth.view.*;
import lab3.labyrinth.solver.*;
import lab3.labyrinth.observer.*;

public class Main {

    public static void main(String[] args) {

        int[][] matrix = {{-1, 0, 0, 1, 1}, {0, 1, 0, 1, 1}, {0, 1, 0, 0, 0}, {0, 1, 1, 1, 2}, {0, 0, 0, 0, 0}};
        Labyrinth labyrinth;
//        labyrinth = LabyrinthFactory.getLabyrinthFromFile("inputFile.txt");

        labyrinth = LabyrinthFactory.getLabyrinthFromMatrix(matrix);

        LabyrinthView labyrinthView = new LabyrinthTextView();
        labyrinthView.setLabyrinth(labyrinth);
        System.out.println(labyrinthView);

//        LabyrinthSolver interactiveSolver = new InteractiveSolver(labyrinth);
//        new InteractiveObserver(interactiveSolver);
//        interactiveSolver.solve();

        LabyrinthSolver automatedSolver = new AutomatedSolver(labyrinth);
        new AutomatedObserver(automatedSolver);
        automatedSolver.solve();

    }
}
